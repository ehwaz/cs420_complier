int g;

void func1 () {
	int c = 2;
	int d = 5;
	
	d = 6;
	return d;
}

int func2 (int a, int b) {
	int e = 2;
	int f = 5;
	
	return func1 ();
}


int main (void) {
	int a = 1;
	int b = 2;
	
	if ( a == 1) {
		func1();
		b = 3;
	}
	else {
		b = 4;
	}
	
	if ( a == 1) {
		b = func2(a, b);
	}
	
	while ( a<5 ) {
		b = b + func1();
		a++;
	}

	for (a = 1; a < 5; ++a) {
		b++;
	}
	
	return b;
}