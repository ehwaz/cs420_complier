import javax.swing.JFrame;

import com.mxgraph.swing.mxGraphComponent;
import com.mxgraph.view.mxGraph;

public class Visualizer extends JFrame {
	private static final long serialVersionUID = -2707712944901661771L;
	static int nodeWidth = 20;
	static int nodeHeight = 20;
	
	public Visualizer() {
		super("SimpleParser");
	}
	
	public void visualizeTree(TreeNode treeGraph) {
		mxGraph graph = new mxGraph();
		Object parent = graph.getDefaultParent();
		
		graph.getModel().beginUpdate();
		try {
			String label = treeGraph.singleValue;
			int numberOfLeaves = (int)Math.pow(2, treeGraph.height); 
			int treeWidth = 20*treeGraph.height;//15*(numberOfLeaves);
			int posXOfRootNode = 50+treeWidth*2; 
			int posYOfRootNode = 20;
			
			Object treeRoot = graph.insertVertex(parent, null, label, posXOfRootNode, posYOfRootNode, nodeWidth, nodeHeight);
			treeGraph.putNodes(graph, parent, posXOfRootNode, posYOfRootNode, treeWidth, treeRoot);
		}
		finally {
			graph.getModel().endUpdate();
		}

		mxGraphComponent graphComponent = new mxGraphComponent(graph);
		getContentPane().add(graphComponent);
	}
}
////
