import java.util.Iterator;
import java.util.Vector;

import com.mxgraph.view.mxGraph;

////Classes for visualization (using JGraph external library)
public class TreeNode {
	public String singleValue;
	public Vector<TreeNode> multipleNodes;
	public String type;
	public TreeNode left;
	public TreeNode right;
	public int height;
	
	TreeNode (String factorStr) {
		singleValue = factorStr;
		multipleNodes = null;
		left 	= null;
		right 	= null;
		height	= 0;
	}
	
	// For HW#2
	TreeNode (String nodeValue, TreeNode leftNode, TreeNode rightNode, String nodeType) {
		singleValue	= nodeValue; 
		multipleNodes = null;
		left 	= leftNode;
		right 	= rightNode;
		type	= nodeType;
		if ( (leftNode != null) && (rightNode != null) ) {
			height	= 1 + ((left.height > right.height) ? left.height : right.height);
		}
		else if (leftNode != null) {
			height	= 1 + leftNode.height;
		}
		else if (rightNode != null) {
			height	= 1 + rightNode.height;
		}
		else {
			System.out.println("Wrong usage of TreeNode()! At least one of leftNode and rightNode should be non-null!");
			height = 1;
		}
	}
	
	TreeNode (String nodeValue, String nodeType) {
		singleValue	= nodeValue; 
		multipleNodes = null;
		left 	= null;
		right 	= null;
		type	= nodeType;
		height	= 1;
	}
	
	TreeNode (Vector<TreeNode> nodeValue) {
		multipleNodes = nodeValue;
		type	= "void";
		singleValue = "List";
		left	= null;
		right	= null;
		
		Iterator<TreeNode> iter = nodeValue.iterator();
		int maxHeight = 0;
		TreeNode curNode;
		
		while (iter.hasNext()) {
			curNode = iter.next();
			if (maxHeight < curNode.height) {
				maxHeight = curNode.height;
			}
		}
		height 	= maxHeight;
	}
	
	static int widthDecrement = 30;
	public void putNodes(mxGraph graph, Object graphParent, int relativeX, int relativeY, int width, Object parentNode) {
		if (left != null && right != null) {
			Object subTreeLeft = graph.insertVertex(graphParent, null, left.singleValue, relativeX-width, relativeY+50, 20, 20);
			graph.insertEdge(graphParent, null, null, parentNode, subTreeLeft);
			left.putNodes(graph, graphParent, relativeX-width, relativeY+50, width/2, subTreeLeft);
			
			Object subTreeRight = graph.insertVertex(graphParent, null, right.singleValue, relativeX+width, relativeY+50, 20, 20);
			graph.insertEdge(graphParent, null, null, parentNode, subTreeRight);
			right.putNodes(graph, graphParent, relativeX+width, relativeY+50, width/2, subTreeRight);
		}
		if (left != null && right == null) {
			Object subTreeLeft = graph.insertVertex(graphParent, null, left.singleValue, relativeX, relativeY+50, 20, 20);
			graph.insertEdge(graphParent, null, null, parentNode, subTreeLeft);
			left.putNodes(graph, graphParent, relativeX, relativeY+50, width, subTreeLeft);
		}
		else if (multipleNodes != null) {
			// put listNode
			Object listNode = graph.insertVertex(graphParent, null, singleValue, relativeX, relativeY+50, 20, 20);
			graph.insertEdge(graphParent, null, null, parentNode, listNode);
			
			// iterate vector to put TreeNodes
			Iterator<TreeNode> iter = multipleNodes.iterator();
			TreeNode curNode;
			int		 numOfSameLvNodes = multipleNodes.size();
			int		 gap = 100;
			int		 xOffset = -(numOfSameLvNodes-1)/2*gap;
			while (iter.hasNext()) {
				curNode = (TreeNode) iter.next();
				listNode = graph.insertVertex(graphParent, null, curNode.singleValue, relativeX+xOffset, relativeY+50, 20, 20);
				graph.insertEdge(graphParent, null, null, parentNode, listNode);
				curNode.putNodes(graph, graphParent, relativeX+xOffset, relativeY+50, width/2, listNode);
				xOffset += gap;
			}
		}
	}
	
	public String toString() {
		if (left != null && right != null) {
			return "{" + singleValue + ": " + left.toString() + ":" + right.toString() + "}";
		}
		else if (left != null && right == null) {
			return "{" + singleValue + ": " + left.toString() + "}";
		}
		else if (multipleNodes != null) {
			return multipleNodes.toString();
		}
		else {
			return "[" + singleValue + "]";
		}
	}
}