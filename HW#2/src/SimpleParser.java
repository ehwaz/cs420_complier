import java.util.*;
 
// for visualization
import javax.swing.JFrame;

//// Class for parsing
public class SimpleParser {
	static String			operator	 = "(\\+|\\-|\\*|\\/|\\;|\\=|\\^|\\<|\\$|\\[|\\]|if|then|while|do|Array|of|mod)";
	static public final String WITH_DELIMITER = "((?<=%1$s)|(?=%1$s))";
	
	static int	   			tokenIndex = 0;
	static int				inputLength = 0;
	static String			input;
	static Vector<String>	tokenVector;
	static Stack<TreeNode>	treeStack;
	
	static HashMap<String, String> declarationMap = new HashMap<String, String>();	// map to store (id type) pair.
	static Vector<TreeNode> resultTreeVector = new Vector<TreeNode>();
	
	public static TreeNode program() throws Exception {
		TreeNode returnNode = null;
		String token = tokenVector.elementAt(tokenIndex);
		System.out.println("declaration() called! Token to process: " + tokenVector.elementAt(tokenIndex));
		declaration();
		
		token = tokenVector.elementAt(tokenIndex);
		if (token.equals("$")) {
			if (tokenIndex < tokenVector.size()-1) {
				tokenIndex++;
				System.out.println("statement() called! Token to process: " + tokenVector.elementAt(tokenIndex));
				returnNode = statement();
			}
		}
		
		return returnNode;
	}

	public static void declaration() throws Exception {
		String token = tokenVector.elementAt(tokenIndex);
		
		if (token.matches("[a-zA-Z]([a-zA-Z]|[0-9])*") && token.matches("^((?!.*(if|then|while|do|array|of|mod)).*)$")) {
			String id = token;
			String type;
			
			if (tokenIndex < tokenVector.size()-1) {
				tokenIndex++;
				
				token = tokenVector.elementAt(tokenIndex);
				if (token.equals("=")) {
					if (tokenIndex < tokenVector.size()-1) {
						tokenIndex++;
						
						System.out.println("assignment() called! Token to process: " + tokenVector.elementAt(tokenIndex));
						type = assignment();
						
						// Add to declaration map
						declarationMap.remove(id);		// remove previous declaration
						declarationMap.put(id, type);	// put new declaration
						
						System.out.println("declaration_prime() called! Token to process: " + tokenVector.elementAt(tokenIndex));
						declaration_prime();
					}
				}
			}
		}
		else {
			System.out.println("declaration_prime() called! Token to process: " + tokenVector.elementAt(tokenIndex));
			declaration_prime();
		}
	}
	
	public static void declaration_prime() throws Exception {
		String token = tokenVector.elementAt(tokenIndex);
		if (token.equals(";")) {
			if (tokenIndex < tokenVector.size()-1) {
				tokenIndex++;
				
				System.out.println("declaration() called! Token to process: " + tokenVector.elementAt(tokenIndex));
				declaration();
				System.out.println("declaration_prime() called! Token to process: " + tokenVector.elementAt(tokenIndex));
				declaration_prime();
			}
		}
		else {
			// or, do nothing (epsilon)
		}
	}
	
	// returns type of term
	public static String assignment() throws Exception {
		String token = tokenVector.elementAt(tokenIndex);
		String retVal = null;
		
		if (token.equals("Boolean") || token.equals("Integer")) {
			// boolean
			if (tokenIndex < tokenVector.size()-1) {
				tokenIndex++;
				
				retVal = token;
			}
		}
		else if (token.equals("^")) {
			// pointer
			if (tokenIndex < tokenVector.size()-1) {
				tokenIndex++;
				
				String type;
				System.out.println("assignment() called! Token to process: " + tokenVector.elementAt(tokenIndex));
				type = assignment();
				
				retVal = "Pointer of " + type;
			}
		}
		else if (token.equals("Array")) {
			// TODO: array
			if (tokenIndex < tokenVector.size()-1) {
				tokenIndex++;
				tokenVector.elementAt(tokenIndex).equals("[");
				tokenIndex++;
				
				token = tokenVector.elementAt(tokenIndex);
				if (token.matches("0|[1-9][0-9]*")) {
					String num = token;
					tokenIndex++;
					
					tokenVector.elementAt(tokenIndex).equals("]");
					tokenIndex++;
					token = tokenVector.elementAt(tokenIndex);
					if (token.equals("of")) {
						tokenIndex++;
						
						String postfixType = assignment();
						retVal = "Array of " + postfixType;
					}
				}
			}
		}
		
		if (retVal == null) {
			throw new Error("Invalid state in assignment()!!");
		}
		
		return retVal;
	}
	
	// Each statement corresponds each tree.
	public static TreeNode statement() throws Exception {
		String token = tokenVector.elementAt(tokenIndex);
		TreeNode returnNode = null;
		
		if (token.matches("[a-zA-Z]([a-zA-Z]|[0-9])*") && token.matches("^((?!.*(if|then|while|do|array|of|mod)).*)$")) {
			String id = token;
			if (tokenIndex < tokenVector.size()-1) {
				tokenIndex++;
				token = tokenVector.elementAt(tokenIndex);
				if (token.equals("=")) {
					if (tokenIndex < tokenVector.size()-1) {
						tokenIndex++;
						
						System.out.println("expression() called! Token to process: " + tokenVector.elementAt(tokenIndex));
						TreeNode resultNode = expression();
						// TODO: check result. type checking b/t id and resultNode
						String idType = declarationMap.get(id);
						
						if (idType == null) {
							throw new Error("\nType error in statement(): no matching declaration for id " + id);
						}
						
						if (idType.equals(resultNode.type) == false) {
							throw new Error("\nType error in statement(): type of id and assignment is different!");
						}
						TreeNode idNode = new TreeNode(id, idType);
						
						System.out.println("statement_prime() called! Token to process: " + tokenVector.elementAt(tokenIndex));
						returnNode = statement_prime(new TreeNode(token, idNode, resultNode, "void"));
					}
				}
			}
		}
		else if (token.equals("if")) {
			if (tokenIndex < tokenVector.size()-1) {
				tokenIndex++;
				
				System.out.println("expression() called! Token to process: " + tokenVector.elementAt(tokenIndex));
				TreeNode conditionNode = expression();
				// TODO: check result. type checking whether resultNode.type is Boolean or not.
				if (conditionNode.type.equals("Boolean") == false) {
					throw new Error("\nType error in statement(): type of condition in if statement is " + conditionNode.type + ", which is not Boolean");
				}
				
				token = tokenVector.elementAt(tokenIndex);
				if (token.equals("then")) {
					if (tokenIndex < tokenVector.size()-1) {
						tokenIndex++;
						
						System.out.println("statement() called! Token to process: " + tokenVector.elementAt(tokenIndex));
						TreeNode bodyNode = statement();
						System.out.println("statement_prime() called! Token to process: " + tokenVector.elementAt(tokenIndex));
						returnNode = statement_prime(new TreeNode("if_then", conditionNode, bodyNode, bodyNode.type));
					}
				}
			}
		}
		else if (token.equals("while")) {
			if (tokenIndex < tokenVector.size()-1) {
				tokenIndex++;
				
				System.out.println("expression() called! Token to process: " + tokenVector.elementAt(tokenIndex));
				TreeNode conditionNode = expression();
				// TODO: check result. type checking whether conditionNode.type is Boolean or not.
				if (conditionNode.type.equals("Boolean") == false) {
					throw new Error("\nType error in statement(): type of condition in while statement is " + conditionNode.type + ", which is not Boolean");
				}
				
				token = tokenVector.elementAt(tokenIndex);
				if (token.equals("do")) {
					if (tokenIndex < tokenVector.size()-1) {
						tokenIndex++;
						
						System.out.println("statement() called! Token to process: " + tokenVector.elementAt(tokenIndex));
						TreeNode bodyNode = statement();
						System.out.println("statement_prime() called! Token to process: " + tokenVector.elementAt(tokenIndex));
						returnNode = statement_prime(new TreeNode("while_do", conditionNode, bodyNode, bodyNode.type));
					}
				}
			}
		}
		
		if (returnNode == null) {
			throw new Error("Parsing error in statement(): no matching case!");
		}
		return returnNode;
	}
	
	public static TreeNode statement_prime(TreeNode inheritedNode) throws Exception {
		String token = tokenVector.elementAt(tokenIndex);
		if (token.equals(";")) {
			if (tokenIndex < tokenVector.size()-1) {
				tokenIndex++;
				
				System.out.println("statement() called! Token to process: " + tokenVector.elementAt(tokenIndex));
				TreeNode statementNode = statement();
				
				if (statementNode.singleValue.equals("List")) {
					if (statementNode.multipleNodes == null) {
						throw new Error("Parsing error in statement_prime(): no list in listNode!");
					}
					
					statementNode.multipleNodes.add(0, inheritedNode);
					System.out.println("statement_prime() called! Token to process: " + tokenVector.elementAt(tokenIndex));
					return statement_prime(statementNode);
				}
				else {
					Vector<TreeNode> nodeList = new Vector<TreeNode>();
					nodeList.add(inheritedNode);
					nodeList.add(statementNode);
					
					TreeNode newListNode = new TreeNode(nodeList);
					return statement_prime(newListNode);
				}
			}
		}
		// or, do nothing (epsilon)
		return inheritedNode;
	}
	
	public static TreeNode expression() throws Exception {
		System.out.println("term() called! Token to process: " + tokenVector.elementAt(tokenIndex));
		TreeNode termNode;
		termNode = term();
		System.out.println("expression_prime called! Token to process: " + tokenVector.elementAt(tokenIndex));
		return expression_prime(termNode);
	}
	
	public static TreeNode expression_prime(TreeNode inheritedNode) throws Exception {
		String token = tokenVector.elementAt(tokenIndex);
		if (token.equals("+") || token.equals("-")) {
			if (tokenIndex < tokenVector.size()-1) {
				tokenIndex++;
				
				TreeNode termNode;
				System.out.println("term() called! Token to process: " + tokenVector.elementAt(tokenIndex));
				termNode = term();
				
				// type checking
				if (inheritedNode.type.equals("Integer") && termNode.type.equals("Integer")) {
					System.out.println("expression_prime() called! Token to process: " + tokenVector.elementAt(tokenIndex));
					return expression_prime(new TreeNode(token, inheritedNode, termNode, "Integer"));
				}
				
				throw new Error("\nType error in expression_prime: inheritedNode type: " + inheritedNode.type + 
								", termNode type: " + termNode.type);
			}
		}
		else if (token.equals("[")) {
			if (tokenIndex < tokenVector.size()-1) {
				tokenIndex++;
				
				TreeNode exprNode;
				
				System.out.println("expression() called! Token to process: " + tokenVector.elementAt(tokenIndex));
				exprNode = expression();
				
				token = tokenVector.elementAt(tokenIndex);
				if (token.equals("]")) {
					if (tokenIndex < tokenVector.size()-1) {
						tokenIndex++;
					}
					// TODO: how to handle type of array??
					// type checking
					String arrayType = declarationMap.get(inheritedNode.singleValue);
					String prefix = "Array of ";
					if (arrayType != null && arrayType.contains(prefix)) {
						String valueType = arrayType.substring(prefix.length());
							
						if (exprNode.type.matches("^(Integer)$")) {
							System.out.println("expression_prime() called! Token to process: " + tokenVector.elementAt(tokenIndex));
							return expression_prime(new TreeNode("[]", inheritedNode, exprNode, valueType));
						}
					}
					throw new Error("\nType error in expression_prime: inheritedNode type: " + inheritedNode.type + 
								", exprNode type: " + exprNode.type);
				}
			}
		}
		
		// or, do nothing (epsilon)
		return inheritedNode;
	}

	public static TreeNode term() throws Exception {
		TreeNode factorNode, returnNode;
		System.out.println("factor() called! Token to process: " + tokenVector.elementAt(tokenIndex));
		factorNode = factor();
		System.out.println("term_prime() called! Token to process: " + tokenVector.elementAt(tokenIndex));
		returnNode = term_prime(factorNode);
		
		// return result
		return returnNode;
	}

	public static TreeNode term_prime(TreeNode inheritedNode) throws Exception {		
		String token = tokenVector.elementAt(tokenIndex);
		if (token.equals("*") || token.equals("/") || token.equals("mod")) {
			if (tokenIndex < tokenVector.size()-1) {
				tokenIndex++;
			}
			
			TreeNode termNode;
			System.out.println("factor() called! Token to process: " + tokenVector.elementAt(tokenIndex));
			termNode = factor();
			
			// type checking
			if (inheritedNode.type.equals("Integer") && termNode.type.equals("Integer")) {
				// return resultNode
				return term_prime(new TreeNode(token, inheritedNode, termNode, "Integer"));
			}
			
			throw new Error("\nType error in term_prime: inheritedNode: " 
							+ inheritedNode.type + ", termNode: " + termNode.type);
		}
		
		return inheritedNode;
	}

	// return leaf node with ID/num and type of ID/num
	public static TreeNode factor() throws Exception {		
		String token = tokenVector.elementAt(tokenIndex); 
		String type;
		if (token.matches("[a-zA-Z]([a-zA-Z]|[0-9])*") && token.matches("^((?!.*(if|then|while|do|array|of|mod)).*)$")) {
			// ID
			if (tokenIndex < tokenVector.size()-1) {
				tokenIndex++;
			}
			type = declarationMap.get(token);
			return new TreeNode(token, type);
		}
		else if (token.matches("0|[1-9][0-9]*")) {
			// num
			if (tokenIndex < tokenVector.size()-1) {
				tokenIndex++;
			}
			type = "Integer";
			return new TreeNode(token, type);
		}
		else if (token.equals("^")) {
			// pointer
			if (tokenIndex < tokenVector.size()-1) {
				tokenIndex++;
			}
			
			System.out.println("factor() called! Token to process: " + tokenVector.elementAt(tokenIndex));
			TreeNode factorNode = factor();
			
			// type checking
			String prefix = "Pointer of ";
			if (factorNode.type.contains(prefix)) {
				// make new node with ^
				return new TreeNode("^", factorNode, null, factorNode.type.substring(prefix.length()));
			}

			throw new Error("\nType error in factor(): factorNode.type: " + factorNode.type + " should be pointer!");
		}
		else {
			throw new Error("Parsing error in factor()! token is " + token);
		}
	}

	public static void main(String[] args) throws Exception {
		System.out.println("# Input code must include two parts; declaration part and statement part.");
		System.out.println("- In declaration part, variables are declared and initialized.");
		System.out.println("- In statement part, variables are manipulated by statements. Only declared variables can be used in statements.");
		System.out.println("- Declaration part and statement part must be separated by character \"$\".");
		System.out.println("- Example: \n\ta=Boolean; b=Boolean; c=Integer; d=Integer; e=^Integer $ if a then while b do c=^e+8; d=9");
		System.out.println("\ta=Array[100] of Integer; b=Integer $ b=a[15]");
		System.out.println("\ta=Integer; b=Integer; c=Integer; d=^^Integer $ a= 1 + a * b mod c - 7 / ^^d");

		System.out.println("\n# Put your code:");
		
		//// Getting input expression and converting to data structure.
		Scanner keyboard = new Scanner(System.in);
		input = keyboard.nextLine();
		input = input.replaceAll("\\s","");	// removing all whitespaces
		// System.out.println(input);
		
		final String[] splitedString = input.split(String.format(WITH_DELIMITER, operator));
		
		tokenVector = new Vector<String>();
		String str;
		for (int i=0; i<splitedString.length; ++i) {
			str = splitedString[i];
			if (str.length() > 0) {
				//System.out.println("Token#" + i + ": " + splitedString[i]);
				tokenVector.addElement(str);
			}
		}
		
		tokenVector = new Vector<String>(Arrays.asList(splitedString));
		treeStack = new Stack<TreeNode>();
		
		inputLength = input.length();
		////
		
		//// Actual parsing
		System.out.println("\n# Parsing...");
		TreeNode returnNode = program();
		System.out.println();
		
		if (returnNode == null) {
			throw new Error("Parsing error: abnormal parsing");
		}
		if (tokenIndex != tokenVector.size()-1) {
			throw new Error("Parsing error: Not all symbols are processed!!\n Index: " + Integer.toString(tokenIndex) + ", Size: " + Integer.toString(tokenVector.size()));
		}
		else {
			System.out.println("# Parsing succeeded!!");
			System.out.println("==== Presentation of declaration table ====");
			System.out.println(declarationMap);
			System.out.println("==== Presentation of ABS in preorder ====");
			System.out.println(returnNode);
		}
		////
		
		//// For visualization
		Visualizer frame = new Visualizer();
		frame.visualizeTree(returnNode);
		
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.setSize(800, 320);
		frame.setVisible(true);
		////
	}
}
////
