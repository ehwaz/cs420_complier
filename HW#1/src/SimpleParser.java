 import java.util.*;
 
 // for visualization
 import javax.swing.JFrame;
 import com.mxgraph.swing.mxGraphComponent;
 import com.mxgraph.view.mxGraph;

//// Classes for visualization (using JGraph external library)
class TreeNode {
	public String singleValue;
	public TreeNode left;
	public TreeNode right;
	public int height;
	
	TreeNode (String factorStr) {
		singleValue = factorStr;
		left 	= null;
		right 	= null;
		height	= 0;
	}
	
	TreeNode (TreeNode leftNode, TreeNode parentNode, TreeNode rightNode) {
		singleValue	= parentNode.singleValue; 
		left 	= leftNode;
		right 	= rightNode;
		height	= 1 + ((left.height > right.height) ? left.height : right.height);;
	}
	
	public void pushToRebuildQueue(LinkedList<TreeNode> queueToRebuild) {
		if (left == null && right == null) {
			queueToRebuild.add(this);
		}
		else {
			left.pushToRebuildQueue(queueToRebuild);
			TreeNode operatorNode = new TreeNode(singleValue);
			queueToRebuild.add(operatorNode);
			queueToRebuild.add(right);
		}
	}
	
	static int widthDecrement = 30;
	public void putNodes(mxGraph graph, Object graphParent, int relativeX, int relativeY, int width, Object parentNode) {
		if (left != null && right != null) {
			Object subTreeLeft = graph.insertVertex(graphParent, null, left.singleValue, relativeX-width, relativeY+50, 20, 20);
			graph.insertEdge(graphParent, null, null, parentNode, subTreeLeft);
			left.putNodes(graph, graphParent, relativeX-width, relativeY+50, width/2, subTreeLeft);
			
			Object subTreeRight = graph.insertVertex(graphParent, null, right.singleValue, relativeX+width, relativeY+50, 20, 20);
			graph.insertEdge(graphParent, null, null, parentNode, subTreeRight);
			right.putNodes(graph, graphParent, relativeX+width, relativeY+50, width/2, subTreeRight);
		}
	}
	
	public String toString() {
		if (left != null && right != null) {
			return "{" + singleValue + ": " + left.toString() + ":" + right.toString() + "}";
		}
		else {
			return "[" + singleValue + "]";
		}
	}
}

class Visualizer extends JFrame {
	private static final long serialVersionUID = -2707712944901661771L;
	static int nodeWidth = 20;
	static int nodeHeight = 20;
	
	public Visualizer() {
		super("SimpleParser");
	}
	
	public void visualizeTree(TreeNode treeGraph) {
		mxGraph graph = new mxGraph();
		Object parent = graph.getDefaultParent();
		
		graph.getModel().beginUpdate();
		try {
			String label = treeGraph.singleValue;
			int numberOfLeaves = (int)Math.pow(2, treeGraph.height); 
			int treeWidth = 5*(numberOfLeaves);
			int posXOfRootNode = 50+treeWidth*2; 
			int posYOfRootNode = 20;
			
			Object treeRoot = graph.insertVertex(parent, null, label, posXOfRootNode, posYOfRootNode, nodeWidth, nodeHeight);
			treeGraph.putNodes(graph, parent, posXOfRootNode, posYOfRootNode, treeWidth, treeRoot);
		}
		finally {
			graph.getModel().endUpdate();
		}

		mxGraphComponent graphComponent = new mxGraphComponent(graph);
		getContentPane().add(graphComponent);
	}
}
////

////Class for parsing
public class SimpleParser {
	static String			operator	 = "(\\+|\\-|\\*|\\/)";
	static public final String WITH_DELIMITER = "((?<=%1$s)|(?=%1$s))";
	
	static int	   			tokenIndex = 0;
	static int				inputLength = 0;
	static String			input;
	static Vector<String>	tokenVector;
	static Stack<TreeNode>	treeStack;
	
	public static void buildSubTreesAndPush() {
		if (treeStack.size() >= 3)
		{
			TreeNode right = treeStack.pop();
			TreeNode parent = treeStack.pop();
			TreeNode left = treeStack.pop();
			
			LinkedList<TreeNode> queueToRebuild = new LinkedList<TreeNode>();
			
//			System.out.println("Building subTree with " + left.singleValue + ", " 
//								+ parent.singleValue + ", " + right.singleValue);
			
			if ( (right.left != null) && (right.right != null) && 
					(parent.singleValue.matches("\\*|\\/")) ) {
				queueToRebuild.add(left);
				queueToRebuild.add(parent);
				right.pushToRebuildQueue(queueToRebuild);
//				System.out.println(queueToRebuild);
				
				while (queueToRebuild.size() > 1) {
					TreeNode rebuildLeft = queueToRebuild.poll();
					TreeNode rebuildParent = queueToRebuild.poll();
					TreeNode rebuildRight = queueToRebuild.poll();
					
					TreeNode newNode = new TreeNode(rebuildLeft, rebuildParent, rebuildRight);
					queueToRebuild.push(newNode);
				}
				
				TreeNode rebuiltNode = queueToRebuild.poll();
				treeStack.push(rebuiltNode);
			}
			else {
				// +, -, and *, / with no child node
				TreeNode nodeToPush = new TreeNode(left, parent, right);
				treeStack.push(nodeToPush);
			}
		}
	}
	
	public static void goal() throws Exception {
		if (inputLength <= tokenIndex) {
			throw new Error("Error in goal()! char is " + Character.toString(input.charAt(tokenIndex)));
		}
//		System.out.println("expr() called! index: " + Integer.toString(tokenIndex));
		expr();
		buildSubTreesAndPush();
	}
	
	public static void expr() throws Exception {
//		System.out.println("term() called!" + "index: " + Integer.toString(tokenIndex));
		term();
//		System.out.println("expr_prime() called! index: " + Integer.toString(tokenIndex));
		expr_prime();
		buildSubTreesAndPush();
	}
	
	public static void expr_prime() throws Exception {
		String token = tokenVector.elementAt(tokenIndex);
		if (token.equals("+") || token.equals("-")) {
//			System.out.println("expr_prime: " + token);
			if (tokenIndex < tokenVector.size()-1) {
				tokenIndex++;
			}
			TreeNode leaf = new TreeNode(token);
			treeStack.push(leaf);
//			System.out.println("expr() called! index: " + Integer.toString(tokenIndex));
			expr();
		}
	}

	public static void term() throws Exception {
//		System.out.println("factor() called! index: " + Integer.toString(tokenIndex));
		factor();
//		System.out.println("term_prime() called! index: " + Integer.toString(tokenIndex));
		term_prime();
		buildSubTreesAndPush();
	}

	public static void term_prime() throws Exception {		
		String token = tokenVector.elementAt(tokenIndex);
		if (token.equals("*") || token.equals("/")) {
//			System.out.println("term_prime: " + token);
			if (tokenIndex < tokenVector.size()-1) {
				tokenIndex++;
			}
			TreeNode leaf = new TreeNode(token);
			treeStack.push(leaf);
//			System.out.println("term() called! index: " + Integer.toString(tokenIndex));
			term();
		}
	}

	public static void factor() throws Exception {		
		String token = tokenVector.elementAt(tokenIndex); 
		if (token.matches("[a-zA-Z]([a-zA-Z]|[0-9])*")) {
			// ID
//			System.out.println("factor-id: " + token);
			TreeNode leaf = new TreeNode(token);
			treeStack.push(leaf);
			if (tokenIndex < tokenVector.size()-1) {
				tokenIndex++;
			}
		}
		else if (token.matches("0|[1-9][0-9]*")) {
			// num
//			System.out.println("factor-num: " + token);
			TreeNode leaf = new TreeNode(token);
			treeStack.push(leaf);
			if (tokenIndex < tokenVector.size()-1) {
				tokenIndex++;
			}
		}
		else {
			throw new Error("Error in factor()! token is " + token);
		}
	}

	public static void main(String[] args) throws Exception {
		System.out.println("Please put an expression without any whitespaces (only +, -, *, /, id and number).");
		
		//// Getting input expression and converting to data structure.
		Scanner keyboard = new Scanner(System.in);
		input = keyboard.nextLine();
		
		final String[] splitedString = input.split(String.format(WITH_DELIMITER, operator));
		
//		tokenVector = new Vector<String>();
//		for (int i=0; i<splitedString.length; ++i) {
//			String str = splitedString[i];
//			System.out.println("Token#" + i + ": " + splitedString[i]);
//			tokenVector.addElement(str);
//		}
		tokenVector = new Vector<String>(Arrays.asList(splitedString));
		treeStack = new Stack<TreeNode>();
		
		inputLength = input.length();
		////
		
		//// Actual parsing
//		System.out.println("goal() called! index: " + Integer.toString(tokenIndex));
		goal();
		
		if (tokenIndex != tokenVector.size()-1) {
			throw new Error("Not all symbols are processed!!\n Index: " + Integer.toString(tokenIndex) + ", Size: " + Integer.toString(tokenVector.size()));
		}
		////
		
		//// For visualization
		TreeNode treeGraph = treeStack.pop();
		System.out.print("Presentation of ABS in preorder: " + treeGraph);
		
		Visualizer frame = new Visualizer();
		frame.visualizeTree(treeGraph);
		
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.setSize(800, 320);
		frame.setVisible(true);
		////
	}
}
////
