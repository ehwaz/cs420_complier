/*
	ANSI C Yacc grammar
	In 1985, Jeff Lee published his Yacc grammar (which is accompanied by a matching Lex specification) for the April 30, 1985 draft version of the ANSI C standard.  Tom Stockfisch reposted it to net.sources in 1987; that original, as mentioned in the answer to question 17.25 of the comp.lang.c FAQ, can be ftp'ed from ftp.uu.net, file usenet/net.sources/ansi.c.grammar.Z.

	Jutta Degener, 1995 
*/

/*
	Translated to CUP grammar by magoon on 2013.5.26
*/

package grammar;

import java_cup.runtime.*;

/* Preliminaries to set up and use the scanner.  */
init with {: scanner.init(); :};
scan with {: return scanner.next_token(); :};

/* Terminals (tokens returned by the scanner). */
terminal IDENTIFIER, CONSTANT, STRING_LITERAL, SIZEOF;
terminal PTR_OP, INC_OP, DEC_OP, LEFT_SHIFT_OP, RIGHT_SHIFT_OP, LE_OP, GE_OP, EQ_OP, NE_OP;
terminal AND_OP, OR_OP, MUL_ASSIGN, DIV_ASSIGN, MOD_ASSIGN, ADD_ASSIGN;
terminal SUB_ASSIGN, LEFT_ASSIGN, RIGHT_ASSIGN, AND_ASSIGN;
terminal XOR_ASSIGN, OR_ASSIGN, TYPE_NAME;

terminal TYPEDEF, EXTERN, STATIC, AUTO, REGISTER;
terminal CHAR, SHORT, INT, LONG, SIGNED, UNSIGNED, FLOAT, DOUBLE, CONST, VOLATILE, VOID;
terminal STRUCT, UNION, ENUM, ELLIPSIS;

terminal CASE, DEFAULT, IF, ELSE, SWITCH, WHILE, DO, FOR, GOTO, CONTINUE, BREAK, RETURN;

terminal SEMI, LEFT_BRACE, RIGHT_BRACE, COMMA, COLON, ASSIGN, LEFT_PAREN, RIGHT_PAREN, LEFT_BRACKET, RIGHT_BRACKET, PERIOD, AND, EXCL, TILDE, MINUS, PLUS, MULTI, DIVIDE, MOD, LEFT_ANGLE, RIGHT_ANGLE, CARET, BAR, QUESTION;

/* Nonterminals */
non terminal primary_expression, postfix_expression, argument_expression_list;
non terminal unary_expression, unary_operator;
non terminal cast_expression, multiplicative_expression, additive_expression, shift_expression, relational_expression, equality_expression;
non terminal and_expression, exclusive_or_expression, inclusive_or_expression, logical_and_expression, logical_or_expression;
non terminal conditional_expression, assignment_expression, assignment_operator;
non terminal expression, constant_expression, declaration, declaration_specifiers, init_declarator_list, init_declarator;
non terminal storage_class_specifier, type_specifier, struct_or_union_specifier, struct_or_union, struct_declaration_list, struct_declaration; 
non terminal specifier_qualifier_list, struct_declarator_list, struct_declarator;
non terminal enum_specifier, enumerator_list, enumerator;
non terminal type_qualifier, declarator, direct_declarator, pointer, type_qualifier_list;
non terminal parameter_type_list, parameter_list, parameter_declaration, identifier_list, type_name;
non terminal abstract_declarator, direct_abstract_declarator, initializer, initializer_list;
non terminal statement, labeled_statement, compound_statement, declaration_list, statement_list;
non terminal expression_statement, selection_statement, iteration_statement, jump_statement;
non terminal translation_unit, external_declaration, function_definition;

start with translation_unit;

primary_expression
	::= IDENTIFIER
	| CONSTANT
	| STRING_LITERAL
	| LEFT_BRACE expression RIGHT_BRACE
	;

postfix_expression
	::= primary_expression
	| postfix_expression LEFT_BRACKET expression RIGHT_BRACKET
	| postfix_expression LEFT_BRACE RIGHT_BRACE
	| postfix_expression LEFT_BRACE argument_expression_list RIGHT_BRACE
	| postfix_expression PERIOD IDENTIFIER
	| postfix_expression PTR_OP IDENTIFIER
	| postfix_expression INC_OP
	| postfix_expression DEC_OP
	;

argument_expression_list
	::= assignment_expression
	| argument_expression_list COMMA assignment_expression
	;

unary_expression
	::= postfix_expression
	| INC_OP unary_expression
	| DEC_OP unary_expression
	| unary_operator cast_expression
	| SIZEOF unary_expression
	| SIZEOF LEFT_BRACE type_name RIGHT_BRACE
	;

unary_operator
	::= AND
	| MULTI
	| PLUS
	| MINUS
	| TILDE
	| EXCL
	;

cast_expression
	::= unary_expression
	| LEFT_BRACE type_name RIGHT_BRACE cast_expression
	;

multiplicative_expression
	::= cast_expression
	| multiplicative_expression MULTI cast_expression
	| multiplicative_expression DIVIDE cast_expression
	| multiplicative_expression MOD cast_expression
	;

additive_expression
	::= multiplicative_expression
	| additive_expression PLUS multiplicative_expression
	| additive_expression MINUS multiplicative_expression
	;

shift_expression
	::= additive_expression
	| shift_expression LEFT_SHIFT_OP additive_expression
	| shift_expression RIGHT_SHIFT_OP additive_expression
	;

relational_expression
	::= shift_expression
	| relational_expression LEFT_ANGLE shift_expression
	| relational_expression RIGHT_ANGLE shift_expression
	| relational_expression LE_OP shift_expression
	| relational_expression GE_OP shift_expression
	;

equality_expression
	::= relational_expression
	| equality_expression EQ_OP relational_expression
	| equality_expression NE_OP relational_expression
	;

and_expression
	::= equality_expression
	| and_expression AND equality_expression
	;

exclusive_or_expression
	::= and_expression
	| exclusive_or_expression CARET and_expression
	;

inclusive_or_expression
	::= exclusive_or_expression
	| inclusive_or_expression BAR exclusive_or_expression
	;

logical_and_expression
	::= inclusive_or_expression
	| logical_and_expression AND_OP inclusive_or_expression
	;

logical_or_expression
	::= logical_and_expression
	| logical_or_expression OR_OP logical_and_expression
	;

conditional_expression
	::= logical_or_expression
	| logical_or_expression QUESTION expression COLON conditional_expression
	;

assignment_expression
	::= conditional_expression
	| unary_expression assignment_operator assignment_expression
	;

assignment_operator
	::= ASSIGN
	| MUL_ASSIGN
	| DIV_ASSIGN
	| MOD_ASSIGN
	| ADD_ASSIGN
	| SUB_ASSIGN
	| LEFT_ASSIGN
	| RIGHT_ASSIGN
	| AND_ASSIGN
	| XOR_ASSIGN
	| OR_ASSIGN
	;

expression
	::= assignment_expression
	| expression COMMA assignment_expression
	;

constant_expression
	::= conditional_expression
	;

declaration
	::= declaration_specifiers SEMI
	| declaration_specifiers init_declarator_list SEMI
	;

declaration_specifiers
	::= storage_class_specifier
	| storage_class_specifier declaration_specifiers
	| type_specifier
	| type_specifier declaration_specifiers
	| type_qualifier
	| type_qualifier declaration_specifiers
	;

init_declarator_list
	::= init_declarator
	| init_declarator_list COMMA init_declarator
	;

init_declarator
	::= declarator
	| declarator ASSIGN initializer
	;

storage_class_specifier
	::= TYPEDEF
	| EXTERN
	| STATIC
	| AUTO
	| REGISTER
	;

type_specifier
	::= VOID
	| CHAR
	| SHORT
	| INT
	| LONG
	| FLOAT
	| DOUBLE
	| SIGNED
	| UNSIGNED
	| struct_or_union_specifier
	| enum_specifier
	| TYPE_NAME
	;

struct_or_union_specifier
	::= struct_or_union IDENTIFIER LEFT_BRACE struct_declaration_list RIGHT_BRACE
	| struct_or_union LEFT_BRACE struct_declaration_list RIGHT_BRACE
	| struct_or_union IDENTIFIER
	;

struct_or_union
	::= STRUCT
	| UNION
	;

struct_declaration_list
	::= struct_declaration
	| struct_declaration_list struct_declaration
	;

struct_declaration
	::= specifier_qualifier_list struct_declarator_list SEMI
	;

specifier_qualifier_list
	::= type_specifier specifier_qualifier_list
	| type_specifier
	| type_qualifier specifier_qualifier_list
	| type_qualifier
	;

struct_declarator_list
	::= struct_declarator
	| struct_declarator_list COMMA struct_declarator
	;

struct_declarator
	::= declarator
	| COLON constant_expression
	| declarator COLON constant_expression
	;

enum_specifier
	::= ENUM LEFT_BRACE enumerator_list RIGHT_BRACE
	| ENUM IDENTIFIER LEFT_BRACE enumerator_list RIGHT_BRACE
	| ENUM IDENTIFIER
	;

enumerator_list
	::= enumerator
	| enumerator_list COMMA enumerator
	;

enumerator
	::= IDENTIFIER
	| IDENTIFIER ASSIGN constant_expression
	;

type_qualifier
	::= CONST
	| VOLATILE
	;

declarator
	::= pointer direct_declarator
	| direct_declarator
	;

direct_declarator
	::= IDENTIFIER
	| LEFT_BRACE declarator RIGHT_BRACE
	| direct_declarator LEFT_BRACKET constant_expression RIGHT_BRACKET
	| direct_declarator LEFT_BRACKET RIGHT_BRACKET
	| direct_declarator LEFT_BRACE parameter_type_list RIGHT_BRACE
	| direct_declarator LEFT_BRACE identifier_list RIGHT_BRACE
	| direct_declarator LEFT_BRACE RIGHT_BRACE
	;

pointer
	::= MULTI
	| MULTI type_qualifier_list
	| MULTI pointer
	| MULTI type_qualifier_list pointer
	;

type_qualifier_list
	::= type_qualifier
	| type_qualifier_list type_qualifier
	;


parameter_type_list
	::= parameter_list
	| parameter_list COMMA ELLIPSIS
	;

parameter_list
	::= parameter_declaration
	| parameter_list COMMA parameter_declaration
	;

parameter_declaration
	::= declaration_specifiers declarator
	| declaration_specifiers abstract_declarator
	| declaration_specifiers
	;

identifier_list
	::= IDENTIFIER
	| identifier_list COMMA IDENTIFIER
	;

type_name
	::= specifier_qualifier_list
	| specifier_qualifier_list abstract_declarator
	;

abstract_declarator
	::= pointer
	| direct_abstract_declarator
	| pointer direct_abstract_declarator
	;

direct_abstract_declarator
	::= LEFT_BRACE abstract_declarator RIGHT_BRACE
	| LEFT_BRACKET RIGHT_BRACKET
	| LEFT_BRACKET constant_expression RIGHT_BRACKET
	| direct_abstract_declarator LEFT_BRACKET RIGHT_BRACKET
	| direct_abstract_declarator LEFT_BRACKET constant_expression RIGHT_BRACKET
	| LEFT_BRACE RIGHT_BRACE
	| LEFT_BRACE parameter_type_list RIGHT_BRACE
	| direct_abstract_declarator LEFT_BRACE RIGHT_BRACE
	| direct_abstract_declarator LEFT_BRACE parameter_type_list RIGHT_BRACE
	;

initializer
	::= assignment_expression
	| LEFT_BRACE initializer_list RIGHT_BRACE
	| LEFT_BRACE initializer_list COMMA RIGHT_BRACE
	;

initializer_list
	::= initializer
	| initializer_list COMMA initializer
	;

statement
	::= labeled_statement
	| compound_statement
	| expression_statement
	| selection_statement
	| iteration_statement
	| jump_statement
	;

labeled_statement
	::= IDENTIFIER COLON statement
	| CASE constant_expression COLON statement
	| DEFAULT COLON statement
	;

compound_statement
	::= LEFT_BRACE RIGHT_BRACE
	| LEFT_BRACE statement_list RIGHT_BRACE
	| LEFT_BRACE declaration_list RIGHT_BRACE
	| LEFT_BRACE declaration_list statement_list RIGHT_BRACE
	;

declaration_list
	::= declaration
	| declaration_list declaration
	;

statement_list
	::= statement
	| statement_list statement
	;

expression_statement
	::= SEMI
	| expression SEMI
	;

selection_statement
	::= IF LEFT_BRACE expression RIGHT_BRACE statement
	| IF LEFT_BRACE expression RIGHT_BRACE statement ELSE statement
	| SWITCH LEFT_BRACE expression RIGHT_BRACE statement
	;

iteration_statement
	::= WHILE LEFT_BRACE expression RIGHT_BRACE statement
	| DO statement WHILE LEFT_BRACE expression RIGHT_BRACE SEMI
	| FOR LEFT_BRACE expression_statement expression_statement RIGHT_BRACE statement
	| FOR LEFT_BRACE expression_statement expression_statement expression RIGHT_BRACE statement
	;

jump_statement
	::= GOTO IDENTIFIER SEMI
	| CONTINUE SEMI
	| BREAK SEMI
	| RETURN SEMI
	| RETURN expression SEMI
	;

translation_unit
	::= external_declaration
	| translation_unit external_declaration
	;

external_declaration
	::= function_definition
	| declaration
	;

function_definition
	::= declaration_specifiers declarator declaration_list compound_statement
	| declaration_specifiers declarator compound_statement
	| declarator declaration_list compound_statement
	| declarator compound_statement
	;