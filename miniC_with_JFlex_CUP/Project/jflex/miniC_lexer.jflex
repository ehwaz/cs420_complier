/*
	ANSI C grammar, Lex specification
	In 1985, Jeff Lee published this Lex specification together with a Yacc grammar for the April 30, 1985 ANSI C draft.  Tom Stockfisch reposted both to net.sources in 1987; that original, as mentioned in the answer to question 17.25 of the comp.lang.c FAQ, can be ftp'ed from ftp.uu.net, file usenet/net.sources/ansi.c.grammar.Z.

	I intend to keep this version as close to the current C Standard grammar as possible; please let me know if you discover discrepancies.

	Jutta Degener, 1995 
*/
/*
	Translated to JFlex grammar by magoon on 2013.5.26
*/

/* --------------------------Usercode Section------------------------ */

import java_cup.runtime.*;

%%

/* -----------------Options and Declarations Section----------------- */

%class Lexer // Name of generated class is Lexer, instead of Yylex
%cup
/* cup 파서의 인터페이스를 구현하겠다는 의미로, 아래와 같은 설정의 효과.
     %implements java_cup.runtime.Scanner	
     	(구문처리기 클래스가 java_cup.runtime.Scanner 를 implements 함.)
     %function next_token			
     	(구문처리기 함수의 이름을 yylex으로부터 next_token 으로 변경함)
     %type java_cup.runtime.Symbol	
     	(구문처리기 함수의 리턴형을 Yytoken으로 부터 java_cup.runtime.Symbol 으로 변경함)
*/

%line		// 줄번호 세기
%column		// 컬럼 번호 세기
%unicode

//%public         // Makes the generated class public
%char           // Turns character counting on. ( enables yychar() )

D		=	[0-9]
L		=	[a-zA-Z_]
H		=	[a-fA-F0-9]
E		=	[Ee][+-]?{D}+
FS		=	(f|F|l|L)
IS		=	(u|U|l|L)*

%{

/* To create a new java_cup.runtime.Symbol with information about
   the current token, the token will have no value in this
   case. */

private Symbol symbol(int type) {
    return new Symbol(type, yyline, yycolumn);
}

/* Also creates a new java_cup.runtime.Symbol with information
   about the current token, but this object has a value. */

private Symbol symbol(int type, Object value) {
    return new Symbol(type, yyline, yycolumn, value);
}

/*
#include <stdio.h>
#include "y.tab.h"

void count();
*/

public int yywrap()
{
	return(1);
}


public void comment()
{
	char c, c1;

//loop:		// TODO: fix it!
/*
	while ((c = input()) != '*' && c != 0)
		putchar(c);

	if ((c1 = input()) != '/' && c != 0)
	{
		unput(c1);
		//goto loop; 	// TODO: fix it!
	}

	if (c != 0)
		putchar(c1);
*/
	while ((c = yycharat(0)) != '*' && c != 0) {}

	if ((c1 = yycharat(0)) != '/' && c != 0)
	{
		yypushback(1);
		//goto loop; 	// TODO: fix it!
	}

	if (c != 0)
		c1 = yycharat(0);
}


int column = 0;

public void count()
{
/*
	int i;
	String str = yytext();

	for (i = 0; str.charAt(i) != '\0'; i++)
		if (str.charAt(i) == '\n')
			column = 0;
		else if (str.charAt(i) == '\t')
			column += 8 - (column % 8);
		else
			column++;
*/
	// ECHO;
	System.out.println(yytext());
}


public Symbol check_type()
{
/*
* pseudo code --- this is what it should check
*
*	if (yytext == type_name)
*		return(TYPE_NAME);
*
*	return(IDENTIFIER);
*/

/*
*	it actually will only return IDENTIFIER
*/

	return symbol(sym.IDENTIFIER, yytext());
}

%}

%%

/*
	{action} 부분에 sym.토큰명 이란 값이 들어가는데, 이는 CUP등과 같은 파서 생성기의 출력으로
	sym.java 파일이 생성되는데, 토큰을 정적 멤버로 갖는 sym 클래스가 정의된다. 따라서 어휘
	분석기인 Lexer(혹은 Scanner)를 만들기 전에 먼저 토큰을 정의해야 한다.
	보통 구문 분석기인 Parser가 어휘 분석기인 Lexer의 토큰 리턴 함수(next_token)를 call  하면
	Lexer는 토큰과 value를 리턴하게 되는데, 보통 객체지향 언어(C++, JAVA등)로 구현된
	Lexer의 경우 value 로 Object 를 반환한다. JFLEX에서는 java_cup.runtime.Symbol 타입의 객체
	를 반환한다. java_cup.runtime.Symbol 은 모든 premitive 형과 Object 형을 지원하므로
	실제 토큰의 값이 뭐가 되건 수용할 수 있다. 
*/

"/*"			{ comment(); }

// "auto"			{ count(); return symbol(sym.AUTO); }
// "break"			{ count(); return symbol(sym.BREAK); }
// "case"			{ count(); return symbol(sym.CASE); }
"char"			{ count(); return symbol(sym.CHAR); }
// "const"			{ count(); return symbol(sym.CONST); }
// "continue"		{ count(); return symbol(sym.CONTINUE); }
// "default"		{ count(); return symbol(sym.DEFAULT); }
// "do"			{ count(); return symbol(sym.DO); }
// "double"		{ count(); return symbol(sym.DOUBLE); }
"else"			{ count(); return symbol(sym.ELSE); }
// "enum"			{ count(); return symbol(sym.ENUM); }
// "extern"		{ count(); return symbol(sym.EXTERN); }
"float"			{ count(); return symbol(sym.FLOAT); }
"for"			{ count(); return symbol(sym.FOR); }
// "goto"			{ count(); return symbol(sym.GOTO); } 	// magoon: goto is not supported.
"if"			{ count(); return symbol(sym.IF); }
"int"			{ count(); return symbol(sym.INT); }
// "long"			{ count(); return symbol(sym.LONG); }
// "register"		{ count(); return symbol(sym.REGISTER); }
"return"		{ count(); return symbol(sym.RETURN); }
// "short"			{ count(); return symbol(sym.SHORT); }
// "signed"		{ count(); return symbol(sym.SIGNED); }
// "sizeof"		{ count(); return symbol(sym.SIZEOF); }
// "static"		{ count(); return symbol(sym.STATIC); }
// "struct"		{ count(); return symbol(sym.STRUCT); }
// "switch"		{ count(); return symbol(sym.SWITCH); }
// "typedef"		{ count(); return symbol(sym.TYPEDEF); }
// "union"			{ count(); return symbol(sym.UNION); }
// "unsigned"		{ count(); return symbol(sym.UNSIGNED); }
"void"			{ count(); return symbol(sym.VOID); }
// "volatile"		{ count(); return symbol(sym.VOLATILE); }
"while"			{ count(); return symbol(sym.WHILE); }

{L}({L}|{D})*		{ count(); return (check_type()); }

//0[xX]{H}+{IS}?		{ count(); return symbol(sym.CONSTANT, yytext()); }
//0{D}+{IS}?		{ count(); return symbol(sym.CONSTANT, yytext()); }
//{D}+{IS}?		{ count(); return symbol(sym.CONSTANT, yytext()); }
//L?'(\\.|[^\\'])+'	{ count(); return symbol(sym.CONSTANT, yytext()); }

//{D}+{E}{FS}?		{ count(); return symbol(sym.CONSTANT, yytext()); }
//{D}*"."{D}+({E})?{FS}?	{ count(); return symbol(sym.CONSTANT, yytext()); }
//{D}+"."{D}*({E})?{FS}?	{ count(); return symbol(sym.CONSTANT, yytext()); }

//[1-9][0-9]*			{ count(); return symbol(sym.CONSTANT_INTEGER, Integer.parseInt(yytext()) ); }
[0-9]*			{ count(); return symbol(sym.CONSTANT_INTEGER, Integer.parseInt(yytext()) ); }
[1-9][0-9]*"."{D}*	{ count(); return symbol(sym.CONSTANT_FLOAT, Float.parseFloat(yytext()) ); }


/* L?\"(\\.|[^\\"])*\"	{ count(); return symbol(sym.STRING_LITERAL, yytext()); } // Former lex grammar */
L?\"(\\.|[^\\\"])*\"	{ count(); return symbol(sym.STRING_LITERAL, yytext()); } // Revised; in [], backslash or quote

// "..."			{ count(); return symbol(sym.ELLIPSIS); }	// magoon: not supported
// ">>="			{ count(); return symbol(sym.RIGHT_ASSIGN); }
// "<<="			{ count(); return symbol(sym.LEFT_ASSIGN); }
// "+="			{ count(); return symbol(sym.ADD_ASSIGN); }
// "-="			{ count(); return symbol(sym.SUB_ASSIGN); }
// "*="			{ count(); return symbol(sym.MUL_ASSIGN); }
// "/="			{ count(); return symbol(sym.DIV_ASSIGN); }
// "%="			{ count(); return symbol(sym.MOD_ASSIGN); }
// "&="			{ count(); return symbol(sym.AND_ASSIGN); }
// "^="			{ count(); return symbol(sym.XOR_ASSIGN); }
// "|="			{ count(); return symbol(sym.OR_ASSIGN); }
// ">>"			{ count(); return symbol(sym.RIGHT_SHIFT_OP); }
// "<<"			{ count(); return symbol(sym.LEFT_SHIFT_OP); }
"++"			{ count(); return symbol(sym.INC_OP); }
"--"			{ count(); return symbol(sym.DEC_OP); }
// "->"			{ count(); return symbol(sym.PTR_OP); }
"&&"			{ count(); return symbol(sym.AND_OP); }
"||"			{ count(); return symbol(sym.OR_OP); }
"<="			{ count(); return symbol(sym.LE_OP); }
">="			{ count(); return symbol(sym.GE_OP); }
"=="			{ count(); return symbol(sym.EQ_OP); }
"!="			{ count(); return symbol(sym.NE_OP); }
";"				{ count(); return symbol(sym.SEMI); }
("{"|"<%")		{ count(); return symbol(sym.LEFT_BRACE); }
("}"|"%>")		{ count(); return symbol(sym.RIGHT_BRACE); }
","			{ count(); return symbol(sym.COMMA); }
// ":"			{ count(); return symbol(sym.COLON); }
"="			{ count(); return symbol(sym.ASSIGN); }
"("			{ count(); return symbol(sym.LEFT_PAREN); }
")"			{ count(); return symbol(sym.RIGHT_PAREN); }
("["|"<:")		{ count(); return symbol(sym.LEFT_BRACKET); }
("]"|":>")		{ count(); return symbol(sym.RIGHT_BRACKET); }
// "."			{ count(); return symbol(sym.PERIOD); }
"&"			{ count(); return symbol(sym.AND); }
"!"			{ count(); return symbol(sym.EXCL); }
// "~"			{ count(); return symbol(sym.TILDE); }			// magoon: not supported
"-"			{ count(); return symbol(sym.MINUS); }
"+"			{ count(); return symbol(sym.PLUS); }
"*"			{ count(); return symbol(sym.MULTI); }
"/"			{ count(); return symbol(sym.DIVIDE); }
"%"			{ count(); return symbol(sym.MOD); }
"<"			{ count(); return symbol(sym.LEFT_ANGLE); }
">"			{ count(); return symbol(sym.RIGHT_ANGLE); }
//"^"			{ count(); return symbol(sym.CARET); }
"|"			{ count(); return symbol(sym.BAR); }
// "?"			{ count(); return symbol(sym.QUESTION); }

[ \t\v\n\f]		{ count(); }
.			{ /* ignore bad characters */ }
