package Classes;

import java.util.*;

public class TableVarSymbol extends TableSymbol {
	// For variable
	public TableVarSymbol(String key, int scope, Type returnType, Object value) {
		this.key 				= key;
		this.value 				= value;
		this.symbolType 		= SymbolType.VARIABLE;
		this.returnType 		= returnType;
		this.pointerLevel 		= 0;
	}
	
	public String toString() {
		String retString = "key: " + key + ", scope: " + scope;

		switch (returnType.typeValue) {
		case CHAR:
			retString += ", value: " + (Character)value + ", type: char";
			break;
			//			case SHORT:
			//				retString += ", value: " + ((Integer)value).toString() + ", type: short";
			//				break;
		case INT:
			retString += ", value: " + ((Integer)value).toString() + ", type: int";
			break;
		case FLOAT:
			retString += ", value: " + ((Float)value).toString() + ", type: float";
			//			case DOUBLE:
			//				retString += ", value: " + ((Float)value).toString() + ", type: double";
			//				break;
		}

		return retString;
	}
}