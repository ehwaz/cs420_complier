package Classes;

import java.util.*;

import Util.TreeNode;
import Util.TreeNode.TreeNodeType;

public class IfNode extends CtrlNode {
	public IfNode (AstNode condition, Node body) {
		super();
		this.ctrlNodetype 	= CtrlNodeType.IF;
		this.condition 		= condition;
		this.body 			= body;
	}
	
	public TreeNode evaluate(SymbolTable table, Boolean forGraphGen) {
		TreeNode conditionNode, bodyNode;
		
		// Actual value evaluation
		if (forGraphGen == false ) {
			conditionNode = condition.evaluate(table, false);
			if (conditionNode.returnValue == 1) {
				bodyNode = ((CompoundNode) body).evaluate(table, false);
			}
		}
		//
		
		// Graph generation
		conditionNode = condition.evaluate(table, true);
		bodyNode = ((CompoundNode) body).evaluate(table, true);
		//
		
		return new TreeNode ("if", conditionNode, bodyNode, null, TreeNodeType.IF);
	}
}