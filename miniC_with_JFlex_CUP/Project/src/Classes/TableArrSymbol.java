package Classes;

import java.util.*;

public class TableArrSymbol extends TableSymbol {
	// For array
	public TableArrSymbol(String key, int scope, Type returnType, 
						Object value, Vector<AstNode> arrayValueList) {
		this.key 				= key;
		this.value 				= value;
		this.symbolType 		= SymbolType.ARRAY;
		this.returnType 		= returnType;
		this.pointerLevel 		= 0;
		
		this.arrayValueList		= arrayValueList;
	}
	
	public String toString() {
		String retString = "key: " + key + ", scope: " + scope;
		retString += ", value: " + (String)value + ", type: function, elementType: " + returnType + "elements: " + arrayValueList.toString();
	
		return retString;
	}
	
	public Vector<AstNode> 		arrayValueList;		// In FUNC, it is argument list. in ARRAY, it is actual array.
}