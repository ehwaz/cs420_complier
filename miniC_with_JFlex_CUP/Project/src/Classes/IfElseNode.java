package Classes;

import java.util.*;

import Util.TreeNode;
import Util.TreeNode.TreeNodeType;

public class IfElseNode extends CtrlNode {
	public IfElseNode (AstNode condition, Node body, Node else_body) {
		super();
		this.ctrlNodetype 	= CtrlNodeType.IF_ELSE;
		this.condition 		= condition;
		this.body 			= body;
		this.else_body 		= else_body;
	}
	
	public TreeNode evaluate(SymbolTable table, Boolean forGraphGen) {
		TreeNode conditionNode, bodyNode, bodyElseNode;
		
		// Actual value evaluation
		if (forGraphGen == false ) {
			conditionNode = condition.evaluate(table, false);
			if (conditionNode.returnValue == 1) {
				bodyNode = ((CompoundNode) body).evaluate(table, false);
			}
			else {
				bodyElseNode = ((CompoundNode) else_body).evaluate(table, false);
			}
		}
		//
		
		// Graph generation
		conditionNode = condition.evaluate(table, true);
		bodyNode = ((CompoundNode) body).evaluate(table, true);
		bodyElseNode = ((CompoundNode) else_body).evaluate(table, true);
		//
		
		return new TreeNode ("if", conditionNode, bodyNode, bodyElseNode, TreeNodeType.IF_ELSE);
	}

	public Node else_body;	// AstNode or CompoundNode
}