package Classes;

import Util.TreeNode;

public class CtrlNode extends Node{
	public enum CtrlNodeType {IF, IF_ELSE, WHILE, FOR, RETURN};
	public CtrlNode () {
		nodeType = NodeType.CTRL;
	}
	
	public TreeNode evaluate(SymbolTable table, Boolean forGraphGen) {
		return null;
	}
	
	public AstNode 		condition;
	public Node			body;
	public CtrlNodeType ctrlNodetype; 
}