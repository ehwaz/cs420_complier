package Classes;

import java.util.*;

import Classes.CtrlNode.CtrlNodeType;
import Util.TreeNode;

public class CompoundNode extends Node {
	public CompoundNode (HashMap<String, TableSymbol> table, Vector<Node> body) {
		this.nodeType 	= Node.NodeType.COMPOUND;
		this.table 		= table;
		this.body 		= body;
	}
	public TreeNode evaluate(SymbolTable table, Boolean forGraphGen) {
		// table = ~~ 와 같이 변경하면 함수 바깥의 table에 영향이 없지만.
		// table.tableVector = ~~ 와 같이 변경하면, 객체가 참조하는 변수를 수정하므로 변경이 가능하다.
		Vector<TreeNode> nodeVector = new Vector<TreeNode>();
		
		// Push local scope first...
		// table.tableVector.add(this.table);
		if (this.table != null) {
			table.pushScope(this.table); // <- 이게 작동하는지 먼저 테스트..
		}
		
		TreeNode childNode;
		int returnValue = Integer.MIN_VALUE;
		
		if (body == null) {
			ReturnNode returnNode = new ReturnNode(null);
			nodeVector.add(returnNode.evaluate(table, forGraphGen));
			return new TreeNode("COMP", nodeVector, returnValue);
		}
		
		Iterator<Node> iter = body.iterator();
		while (iter.hasNext()) {
			Node currentNode = (Node) iter.next();
			
			switch(currentNode.nodeType) {
			case AST:
				childNode = ((AstNode)currentNode).evaluate(table, forGraphGen);
				nodeVector.add(childNode);
				break;
			case CTRL:
				CtrlNode ctrlNode = (CtrlNode)currentNode;
				childNode = ctrlNode.evaluate(table, forGraphGen);
				nodeVector.add(childNode);
				if (ctrlNode.ctrlNodetype == CtrlNodeType.RETURN) {
					returnValue = childNode.returnValue;
				}
				break;
			case COMPOUND:
				childNode = ((CompoundNode)currentNode).evaluate(table, forGraphGen);
				nodeVector.add(childNode);
				if (childNode.returnValue != Integer.MIN_VALUE) {
					returnValue = childNode.returnValue;
				}
				break;
			}
		}
		
		// Pop local scope...
		if (this.table != null) {
			table.popScope();
		}
		
		return new TreeNode("COMP", nodeVector, returnValue);
	}
	
	public HashMap<String, TableSymbol> table;
	public Vector<Node> body;
}
