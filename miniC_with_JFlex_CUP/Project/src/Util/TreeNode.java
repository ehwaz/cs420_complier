package Util;

import java.util.Iterator;
import java.util.Vector;

import com.mxgraph.view.mxGraph;

////Classes for visualization (using JGraph external library)
public class TreeNode {
	public enum TreeNodeType {AST, IF, IF_ELSE, WHILE, FOR, RETURN, COMP};
	
	public String singleValue;
	public TreeNodeType type;
	
	public TreeNode declaration;
	public TreeNode condition;
	public TreeNode inc;
	
	public TreeNode body;
	public TreeNode elseBody;
	public Vector<TreeNode> multipleNodes;
	
	public int returnValue;
	
	public int height;
	
	static int gap = 50;
	
	// For astNode
	public TreeNode (String factorStr, int returnValue) {
		this.singleValue 	= factorStr;
		this.type 			= TreeNodeType.AST;
		this.declaration 	= null;
		this.condition 		= null;
		this.inc			= null;
		
		this.body			= null;
		this.elseBody		= null;
		this.multipleNodes 	= null;
		
		this.returnValue		= returnValue;
	}
	
	// return Node
		public TreeNode (String nodeName, TreeNode returnNode, int returnValue) {
			this.singleValue 	= nodeName;
			this.type 			= TreeNodeType.RETURN;
			this.declaration 	= null;
			this.condition 		= returnNode;
			this.inc			= null;
			
			this.body			= null;
			this.elseBody		= null;
			this.multipleNodes 	= null;

			this.returnValue		= returnValue;
		}
		
	// if, while, if else Node
	public TreeNode (String nodeName, TreeNode condition, TreeNode body, TreeNode elseBody, TreeNodeType type) {
		this.singleValue 	= nodeName;
		this.type 			= type;
		this.declaration 	= null;
		this.condition 		= condition;
		this.inc			= null;
		
		this.body			= body;
		this.elseBody		= elseBody;
		this.multipleNodes 	= null;

		this.returnValue		= Integer.MIN_VALUE;
	}
	
	// for for Node
	public TreeNode (String nodeName, TreeNode declaration, TreeNode condition, TreeNode inc, TreeNode body) {
		this.singleValue 	= nodeName;
		this.type 			= TreeNodeType.FOR;
		this.declaration 	= declaration;
		this.condition 		= condition;
		this.inc				= inc;
		
		this.body			= body;
		this.elseBody		= null;
		this.multipleNodes 	= null;

		this.returnValue		= Integer.MIN_VALUE;
	}
	
	// for Comp Node
	public TreeNode (String nodeName, Vector<TreeNode> multipleNodes, int returnValue) {
		this.singleValue 	= nodeName;
		this.type 			= TreeNodeType.COMP;
		this.declaration 	= null;
		this.condition 		= null;
		this.inc				= null;
		
		this.body			= null;
		this.elseBody		= null;
		this.multipleNodes 	= multipleNodes;

		this.returnValue		= returnValue;
	}
	
	class ReturnFormat {
		public Object 	graphNode;
		public int		graphHeight;
		
		public ReturnFormat (Object graphNode, int graphHeight) {
			this.graphNode = graphNode;
			this.graphHeight = graphHeight;
		}
	}

	public Vector<ReturnFormat> putNodes(mxGraph graph, Object graphParent, int relativeX, int relativeY, int width, Object parentNode) {
		Object conditionNode, bodyNode, elseBodyNode;
		Object declarationNode, incNode;
		int height = gap;
		
		Vector<ReturnFormat> vectorToReturn = null;
		Vector<ReturnFormat> tempVector;
		
		Iterator<ReturnFormat> iter1;
		
		switch (type) {
		case AST:
			if (body != null) {
				relativeY += gap;
				bodyNode = graph.insertVertex(graphParent, null, body.singleValue, relativeX, relativeY, 80, 25);
				graph.insertEdge(graphParent, null, null, parentNode, bodyNode);
				
				vectorToReturn = body.putNodes(graph, graphParent, relativeX, relativeY, width, bodyNode);
			}
			else {
				height = relativeY + gap;
				vectorToReturn = new Vector<ReturnFormat>();
				vectorToReturn.add(new ReturnFormat(parentNode, height));
			}
			break;
		case RETURN:
			if (condition != null) {
				relativeY += gap;
				bodyNode = graph.insertVertex(graphParent, null, condition.singleValue, relativeX, relativeY, 80, 25);
				graph.insertEdge(graphParent, null, null, parentNode, bodyNode);
				
				vectorToReturn = condition.putNodes(graph, graphParent, relativeX, relativeY, width, bodyNode);
			}
			else {
				height = relativeY + gap;
				vectorToReturn = new Vector<ReturnFormat>();
				vectorToReturn.add(new ReturnFormat(parentNode, height));
			}
			break;
		case IF:
			relativeY += gap;
			conditionNode = graph.insertVertex(graphParent, null, condition.singleValue, relativeX, relativeY, 80, 25, "shape=rhombus");
			graph.insertEdge(graphParent, null, null, parentNode, conditionNode);
			
			relativeY += gap;
			width /= 2;
			bodyNode = graph.insertVertex(graphParent, null, body.singleValue, relativeX-width, relativeY, 80, 25);
			graph.insertEdge(graphParent, null, "true", conditionNode, bodyNode);
			
			vectorToReturn = body.putNodes(graph, graphParent, relativeX-width, relativeY, width, bodyNode);
			vectorToReturn.add(new ReturnFormat(conditionNode, relativeY));
			break;
		case IF_ELSE:
			relativeY += gap;
			conditionNode = graph.insertVertex(graphParent, null, condition.singleValue, relativeX, relativeY, 80, 25, "shape=rhombus");
			graph.insertEdge(graphParent, null, null, parentNode, conditionNode);
			
			relativeY += gap;
			width /= 2;
			bodyNode = graph.insertVertex(graphParent, null, body.singleValue, relativeX-width, relativeY, 80, 25);
			graph.insertEdge(graphParent, null, "true", conditionNode, bodyNode);
			
			elseBodyNode = graph.insertVertex(graphParent, null, elseBody.singleValue, relativeX+width, relativeY, 80, 25);
			graph.insertEdge(graphParent, null, "false", conditionNode, elseBodyNode);
			
			vectorToReturn = new Vector<ReturnFormat>();
			
			tempVector = body.putNodes(graph, graphParent, relativeX-width, relativeY, width, bodyNode);
			vectorToReturn.addAll(tempVector);
			tempVector = elseBody.putNodes(graph, graphParent, relativeX+width, relativeY, width, elseBodyNode);
			vectorToReturn.addAll(tempVector);
			break;
		case WHILE:
			relativeY += gap;
			conditionNode = graph.insertVertex(graphParent, null, condition.singleValue, relativeX, relativeY, 80, 25, "shape=rhombus");
			graph.insertEdge(graphParent, null, null, parentNode, conditionNode);
			
			relativeY += gap;
			bodyNode = graph.insertVertex(graphParent, null, body.singleValue, relativeX+width, relativeY, 80, 25);
			graph.insertEdge(graphParent, null, "then", conditionNode, bodyNode, "rounded=1;edgeStyle=orthogonalEdgeStyle");
			graph.insertEdge(graphParent, null, null, bodyNode, conditionNode, "rounded=1;edgeStyle=orthogonalEdgeStyle");
			
			tempVector = body.putNodes(graph, graphParent, relativeX+width, relativeY, width, bodyNode);
			iter1 = tempVector.iterator();
			while (iter1.hasNext()) {
				ReturnFormat format = iter1.next();
				height = (format.graphHeight > height) ? format.graphHeight : height;
			}
			
			vectorToReturn = new Vector<ReturnFormat>();
			vectorToReturn.add(new ReturnFormat(conditionNode, height));
			break;
		case FOR:
			relativeY += gap;
			declarationNode = graph.insertVertex(graphParent, null, declaration.singleValue, relativeX, relativeY, 80, 25);
			graph.insertEdge(graphParent, null, null, parentNode, declarationNode);
			
			relativeY += gap;
			conditionNode = graph.insertVertex(graphParent, null, condition.singleValue, relativeX, relativeY, 80, 25, "shape=rhombus");
			graph.insertEdge(graphParent, null, null, declarationNode, conditionNode);
			
			relativeY += 30;
			bodyNode = graph.insertVertex(graphParent, null, body.singleValue, relativeX+width, relativeY, 80, 25);
			graph.insertEdge(graphParent, null, "then", conditionNode, bodyNode);
			
			relativeY -= 60;
			incNode = graph.insertVertex(graphParent, null, inc.singleValue, relativeX+width, relativeY, 80, 25);
			graph.insertEdge(graphParent, null, null, bodyNode, incNode);
			graph.insertEdge(graphParent, null, null, incNode, conditionNode);
			
			relativeY += gap+30;
			tempVector = body.putNodes(graph, graphParent, relativeX+width, relativeY, width, bodyNode);
			iter1 = tempVector.iterator();
			while (iter1.hasNext()) {
				ReturnFormat format = iter1.next();
				height = (format.graphHeight > height) ? format.graphHeight : height;
			}
			
			vectorToReturn = new Vector<ReturnFormat>();
			vectorToReturn.add(new ReturnFormat(conditionNode, height));
			break;
		case COMP:
			// iterate vector to put TreeNodes
			Iterator<TreeNode> iter2 = multipleNodes.iterator();
			TreeNode curNode;
			Vector<ReturnFormat> formerRetVector = new Vector<ReturnFormat>();
			Object listNode;
			height = relativeY;
			
			formerRetVector.add(new ReturnFormat(parentNode, relativeY));
			while (iter2.hasNext()) {
				curNode = (TreeNode) iter2.next();
				if (curNode != null) {
					
					iter1 = formerRetVector.iterator();
					while (iter1.hasNext()) {
						ReturnFormat retFormat = iter1.next();
						height = (retFormat.graphHeight > height) ? retFormat.graphHeight : height;
					}
					
					height += gap;
					listNode = graph.insertVertex(graphParent, null, curNode.singleValue, relativeX, height, 80, 25);
					
					iter1 = formerRetVector.iterator();
					while (iter1.hasNext()) {
						ReturnFormat retFormat = iter1.next();
						graph.insertEdge(graphParent, null, null, retFormat.graphNode, listNode);
					}
					//height += gap;
					formerRetVector = curNode.putNodes(graph, graphParent, relativeX, height, width, listNode);
				}
				vectorToReturn = new Vector<ReturnFormat>();
				vectorToReturn.addAll(formerRetVector);
			}
			break;
		default:
			//height += 1;
			break;
		}
		
		return vectorToReturn;
	}
	/*
	public String toString() {
		if (left != null && right != null) {
			return "{" + singleValue + ": " + left.toString() + ":" + right.toString() + "}";
		}
		else if (left != null && right == null) {
			return "{" + singleValue + ": " + left.toString() + "}";
		}
		else if (multipleNodes != null) {
			return multipleNodes.toString();
		}
		else {
			return "[" + singleValue + "]";
		}
	}
	*/
}