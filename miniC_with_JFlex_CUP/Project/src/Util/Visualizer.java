package Util;

import javax.swing.JFrame;

import Classes.CompoundNode;
import Classes.SymbolTable;
import Classes.TableFuncSymbol;
import Classes.TableSymbol;
import Classes.TableSymbol.SymbolType;

import com.mxgraph.swing.mxGraphComponent;
import com.mxgraph.util.mxConstants;
import com.mxgraph.view.mxGraph;
import com.mxgraph.view.mxStylesheet;

import java.util.*;

public class Visualizer extends JFrame {
	private static final long serialVersionUID = -2707712944901661771L;
	static int nodeWidth = 80;
	static int nodeHeight = 25;
	
	mxGraph graph;
	mxStylesheet foo;
	
	public Visualizer() {
		super("miniC_interpretor");
		
		graph = new mxGraph();
		/*
		Object parent = graph.getDefaultParent();
		
		// Settings for edges
	    Map<String, Object> edge = new HashMap<String, Object>();
	    edge.put(mxConstants.STYLE_ROUNDED, true);
	    edge.put(mxConstants.STYLE_ORTHOGONAL, false);
	    //edge.put(mxConstants.STYLE_EDGE, "elbowEdgeStyle");
	    edge.put(mxConstants.STYLE_SHAPE, mxConstants.SHAPE_CONNECTOR);
	    edge.put(mxConstants.STYLE_ENDARROW, mxConstants.ARROW_CLASSIC);
	    edge.put(mxConstants.STYLE_VERTICAL_ALIGN, mxConstants.ALIGN_MIDDLE);
	    edge.put(mxConstants.STYLE_ALIGN, mxConstants.ALIGN_CENTER);
	    edge.put(mxConstants.STYLE_STROKECOLOR, "#000000"); // default is #6482B9
	    edge.put(mxConstants.STYLE_FONTCOLOR, "#446299");

	    mxStylesheet edgeStyle = new mxStylesheet();
	    edgeStyle.setDefaultEdgeStyle(edge);
	    graph.setStylesheet(edgeStyle);
		*/
		/*
		graph.getModel().beginUpdate();
		try
		{
			Object v1 = graph.insertVertex(parent, null, "Hello", 20, 20, 80,
					30);
			Object v2 = graph.insertVertex(parent, null, "World!", 240, 150,
					80, 30);
			graph.insertEdge(parent, null, "Edge", v1, v2);
			graph.insertEdge(parent, null, "Loop", v2, v2);
		}
		finally
		{
			graph.getModel().endUpdate();
		}
		*/
		mxGraphComponent graphComponent = new mxGraphComponent(graph);
		getContentPane().add(graphComponent);
		
	}
	
	public void visualizeTree(SymbolTable table) {
		Object parent = graph.getDefaultParent();
		
		graph.getModel().beginUpdate();
		try {
			
			TableSymbol symbol = table.findSymbol("main", 0);
            if ( (symbol == null) || (symbol.symbolType != SymbolType.FUNC) ) {
            	System.err.println("No function with name \"main\"!!");
            	return;
            }
            
            System.out.println("Main function is called!!");
            
            CompoundNode functionBody = ((TableFuncSymbol) symbol).getFunctionBody();
            TreeNode nodeToDraw = functionBody.evaluate(table, false);
			
			String label = "main()"; //treeGraph.singleValue;
			//int numberOfLeaves = (int)Math.pow(2, treeGraph.height); 
			int treeWidth = 200; //20*treeGraph.height;//15*(numberOfLeaves);
			int posXOfRootNode = 200; 
			int posYOfRootNode = 20;
			
			if (nodeToDraw.returnValue != Integer.MIN_VALUE) {
				System.out.println("Main() is returned with value " + nodeToDraw.returnValue + "!");
			}
			else {
				System.out.println("Main() is returned with no value!");
			}
			
			
			Object treeRoot = graph.insertVertex(parent, null, label, posXOfRootNode, posYOfRootNode, nodeWidth, nodeHeight);
			nodeToDraw.putNodes(graph, parent, posXOfRootNode, posYOfRootNode, treeWidth, treeRoot);
		}
		finally {
			graph.getModel().endUpdate();
		}
	}
}
////
