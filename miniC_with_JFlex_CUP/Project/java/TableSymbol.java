import java.util.*;

public class TableSymbol {
	public enum SymbolType {VOID, CHAR, INT, FLOAT, FUNC, ARRAY};
	
	public static class Type {
		public Type (SymbolType typeValue, int pointerLevel) {
			this.typeValue = typeValue;
			this.pointerLevel = pointerLevel;
		}
		
		SymbolType typeValue;
		int	pointerLevel;
	}
	
	public static class ArgNode {
		public ArgNode(Type argType, Object argValue) {
			this.argType = argType;
			this.argValue = argValue;
		}
		
		Type 	argType;
		Object	argValue;
	}
	
	public TableSymbol(String key, int scope, Type type, Object value) {
		if (type.typeValue == SymbolType.FUNC) {
			System.out.println("Error in TableSymbol: Use other constructor for func!");
			return;
		}
		
		this.key = key;
		this.value = value;
		this.type = type;
		this.subType = null;
		this.subVector = null;
	}
	
	public TableSymbol(String key, int scope, Type type, Object value, 
						Type subType, Vector<ArgNode> subVector) {
		this.key = key;
		this.value = value;
		this.type = type;
		this.subType = subType;
		this.subVector = subVector;
	}
	
	public String toString() {
		String retString = "key: " + key + ", scope: " + scope;
		
		switch (type.typeValue) {
			case FUNC:
				retString += ", value: " + (String)value + ", type: function, returnType: " + subType + "arguments: " + subVector.toString();
				break;
			case ARRAY:
				retString += ", value: " + (String)value + ", type: function, elementType: " + subType + "elements: " + subVector.toString();
				break;
			case CHAR:
				retString += ", value: " + (Character)value + ", type: char";
				break;
//			case SHORT:
//				retString += ", value: " + ((Integer)value).toString() + ", type: short";
//				break;
			case INT:
				retString += ", value: " + ((Integer)value).toString() + ", type: int";
				break;
			case FLOAT:
				retString += ", value: " + ((Float)value).toString() + ", type: float";
//			case DOUBLE:
//				retString += ", value: " + ((Float)value).toString() + ", type: double";
//				break;
			default:
				System.out.println("Error: no matching type for symbol");
				return null;
		}

		return retString;
	}
	
	String 		key;
	Object 		value;
	Type		type;
	int			scope;
	
	// In case of FUNC or ARRAY
	Type 		subType;		// In FUNC, it is return type. In ARRAY, it is array element type.
	Vector<ArgNode> subVector;	// In FUNC, it is argument list. In ARRAY, it is actual array.
}