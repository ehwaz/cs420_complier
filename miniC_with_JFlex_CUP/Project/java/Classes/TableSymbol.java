package Classes;

import java.util.*;

public class TableSymbol {
	public enum SymbolType {VARIABLE, FUNC, ARRAY};
	public enum ReturnType {VOID, CHAR, INT, FLOAT, STRING};
	
	public static class Type {
		public Type (ReturnType typeValue, int pointerLevel) {
			this.typeValue = typeValue;
			this.pointerLevel = pointerLevel;
		}
		
		public String toString() {
			String retString = "";
			
			switch (typeValue) {
			case VOID:
				retString += "void";
				break;
			case CHAR:
				retString += "char";
				break;
			case INT:
				retString += "int";
				break;
			case FLOAT:
				retString += "float";
				break;
			}
			
			for (int i=0; i<pointerLevel; ++i) {
				retString += "*";
			}
			
			return retString;
		}
		
		public ReturnType typeValue;
		public int	pointerLevel;
	}
	
	public static class ArgNode {
		public ArgNode(Type argType, String argName, Object argValue) {
			this.argType = argType;
			this.argName = argName;
			this.argValue = argValue;
		}
		
		public Type 	argType;
		public String	argName;
		public Object	argValue;
	}
	
	public String 		key;
	public Object 		value;		// In FUNC, is content of function(Vector<Node>). in array, it is length of array. (Integer)
	public Type 		returnType;			// In FUNC, it is return type. In ARRAY, it is array element type.
	public SymbolType	symbolType;
	public int			scope;
	public int			pointerLevel;
}