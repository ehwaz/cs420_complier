package Classes;
import Classes.TableSymbol.*;

import java.util.*;

public class SymbolTable {
	public SymbolTable() 
	{
		HashMap<String, TableSymbol> globalTable;
		tableVector = new Vector<HashMap<String, TableSymbol>>();
		
		globalTable = new HashMap<String, TableSymbol>();
		tableVector.add(globalTable);
	}
	
	public void insertSymbol(String key, int scope, TableSymbol symbol) 
	{
		HashMap<String, TableSymbol> tableAtScope = tableVector.elementAt(scope);
		tableAtScope.put(key, symbol);
	}
	/*
	public void insertSymbol(String key, int scope, Type type, Object value) 
	{
		if (type.typeValue == SymbolType.FUNC) {
			System.out.println("Error in insertSymbol: Use other constructor for func!");
			return;
		}
		
		HashMap<String, TableSymbol> tableAtScope = tableVector.elementAt(scope);
		TableSymbol symbol = new TableSymbol(key, scope, type, value);
		tableAtScope.put(key, symbol);
	}
	
	public void insertSymbol(String key, Type type, Object value, 
								Type returnType, Vector<ArgNode> argVector, int scope) 
	{
		assert type.typeValue == SymbolType.FUNC;
		
		HashMap<String, TableSymbol> tableAtScope = tableVector.elementAt(scope);
		TableSymbol symbol = new TableSymbol(key, scope, type, value, returnType, argVector);
		
		System.out.println("SymbolTable::insertSymbol: symbol " + symbol.toString() + " is inserted!");
		tableAtScope.put(key, symbol);
	}
	*/
	public TableSymbol findSymbol(String name, int scope) 
	{
		HashMap<String, TableSymbol> tableAtScope;
		TableSymbol ret = null;
		
		Iterator<HashMap<String, TableSymbol>> iter = tableVector.iterator();
		
		// find symbol from tableStack (from top of Stack);
		while (iter.hasNext()) {
			tableAtScope = iter.next();
			ret = tableAtScope.get(name);
			if (ret != null) {
				break;
			}
		}
		
		//tableAtScope = tableVector.elementAt(0);
		//ret = tableAtScope.get(name);
		
		return ret;
	}
	
	public void pushScope() {
		tableVector.add(new HashMap<String, TableSymbol>());
	}
	
	public void pushScope(HashMap<String, TableSymbol> scope) {
		tableVector.insertElementAt(scope, 0);
		//tableVector.add(scope);
	}
	
	public void popScope() {
		// delete most recent scope stack. global stack shouldn't be popped.
		if (tableVector != null && (tableVector.size() != 1)) {
			tableVector.removeElementAt(0);
		}
		else {
			if (tableVector.size() == 1) {
				System.out.println("Error, SymbolTable::popScope(): why popping global table?");
			}
			else {
				System.out.println("Error, SymbolTable::popScope(): no scope to pop!");
			}
		}
	}
	
	public Vector<HashMap<String, TableSymbol>> tableVector;
}
