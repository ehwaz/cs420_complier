package Classes;

import java.util.*;

import Classes.TableSymbol.*;

public class DeclarationNode {
	public enum DeclarationType {VOID, FUNCTION, ARRAY, VARIABLE};
	
	public DeclarationNode () {
		this.type = DeclarationType.VOID;
		this.name = null;
		this.upperNode = null;
		this.value = null;
		this.paramList = null;
		this.pointerLevel = -1;
	}
	
	public DeclarationNode (String name, AstNode value) {
		this.type = DeclarationType.VARIABLE;
		this.name = name;
		this.upperNode = null;
		this.value = value;
		this.paramList = null;
		this.pointerLevel = 0;
	}
	
	public DeclarationNode (DeclarationNode upperNode, AstNode index) {
		this.type = DeclarationType.ARRAY;
		this.name = null;
		this.upperNode = upperNode;
		this.value = index;
		this.paramList = null;
		this.pointerLevel = 0;
	}
	
	public DeclarationNode (String name, Vector<ArgNode> paramList) {
		this.type = DeclarationType.FUNCTION;
		this.name = name;
		this.upperNode = null;
		this.value = null;
		this.paramList = paramList;
		this.pointerLevel = 0;
	}
	
	public DeclarationType 	type;
	
	public String		 		name;
	public DeclarationNode		upperNode;
	
	public AstNode				value;	// for array, it is index (AstNode), for variable, it is value 
	public Vector<ArgNode> 		paramList;
	public int					pointerLevel;
}
