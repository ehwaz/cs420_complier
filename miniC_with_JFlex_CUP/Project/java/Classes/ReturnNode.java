package Classes;

import Util.TreeNode;
import Util.TreeNode.TreeNodeType;

public class ReturnNode extends CtrlNode {
	public ReturnNode (AstNode body) {
		super();
		this.ctrlNodetype 	= CtrlNodeType.RETURN;
		this.condition 		= body;			// in return node, it is return value..
		this.body 			= null;
	}
	
	public TreeNode evaluate(SymbolTable table, Boolean forGraphGen) {
		TreeNode retNode = null;
		
		if (condition != null) {
			TreeNode returnNode = condition.evaluate(table, forGraphGen);
			retNode = new TreeNode ("return", returnNode, returnNode.returnValue);
			//System.out.println("Function is returned with value " + returnNode.returnValue + "!");
		}
		else {
			retNode = new TreeNode ("return", (TreeNode)null, Integer.MIN_VALUE);
		}
		
		return retNode;
	}
}