package Classes;

import Util.TreeNode;
import Util.TreeNode.TreeNodeType;

public class WhileNode extends CtrlNode {
	public WhileNode (AstNode condition, Node body) {
		super();
		this.ctrlNodetype	= CtrlNodeType.WHILE;
		this.condition	 	= condition;
		this.body 			= body;
	}
	
	public TreeNode evaluate(SymbolTable table, Boolean forGraphGen) {
		TreeNode conditionNode, bodyNode;
		
		// Actual value evaluation
		if (forGraphGen == false ) {
			conditionNode = condition.evaluate(table, false);
			while (conditionNode.returnValue == 1) {
				bodyNode 	= ((CompoundNode) body).evaluate(table, false);
				conditionNode 	= condition.evaluate(table, false);
			}
		}
		//
		
		// Graph generation
		conditionNode = condition.evaluate(table, true);
		bodyNode		= ((CompoundNode) body).evaluate(table, true);
		//
		
		return new TreeNode ("while", conditionNode, bodyNode, null, TreeNodeType.WHILE);
	}
}