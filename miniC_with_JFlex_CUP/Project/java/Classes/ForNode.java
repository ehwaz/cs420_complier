package Classes;

import Util.TreeNode;

public class ForNode extends CtrlNode {
	public ForNode (AstNode decl, AstNode condition, AstNode inc, Node body) {
		super();
		this.ctrlNodetype 	= CtrlNodeType.FOR;
		this.declaration	= decl;
		this.condition 		= condition;
		this.inc 			= inc;
		this.body 			= body;
	}
	
	public TreeNode evaluate(SymbolTable table, Boolean forGraphGen) {
		TreeNode declarationNode, conditionNode, incNode, bodyNode;
		
		// Actual value evaluation
		if (forGraphGen == false ) {
			declarationNode = declaration.evaluate(table, false);
			conditionNode 	= condition.evaluate(table, false);
			while (conditionNode.returnValue == 1) {
				bodyNode 	= ((CompoundNode) body).evaluate(table, false);
				incNode 	= inc.evaluate(table, false);
				conditionNode 	= condition.evaluate(table, false);
			}	
		}
		//
		
		// Graph generation
		declarationNode = declaration.evaluate(table, true);
		conditionNode 	= condition.evaluate(table, true);
		incNode			= inc.evaluate(table, true);
		bodyNode		= ((CompoundNode) body).evaluate(table, true);
		//
		
		return new TreeNode ("for", declarationNode, conditionNode, incNode, bodyNode);
	}
	
	public AstNode 		declaration;
	public AstNode 		inc;
	//Node 				body;		// AstNode or CompoundNode
}