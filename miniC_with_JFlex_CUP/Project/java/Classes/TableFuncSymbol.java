package Classes;

import java.util.*;

public class TableFuncSymbol extends TableSymbol {
	// For function
	public TableFuncSymbol(String key, int scope, Type returnType, Object value, 
						Vector<TableSymbol> argList, HashMap<String, TableSymbol> declarationInfo) {
		this.key 				= key;
		this.value 				= value;
		this.symbolType 		= SymbolType.FUNC;
		this.returnType 		= returnType;
		this.pointerLevel 		= 0;
		
		this.argList 			= argList;
		this.declarationInfo 	= declarationInfo;
	}
	
	public String toString() {
		String retString = "key: " + key + ", scope: " + scope;
		retString += ", value: " + (String)value + ", type: function, returnType: " + returnType + "arguments: " + argList.toString();
	
		return retString;
	}
	
	public CompoundNode getFunctionBody() {
		return (CompoundNode) value;
	}
	
	// value is CompoundNode
	public Vector<TableSymbol> 			argList;		// In FUNC, it is argument list.
	public HashMap<String, TableSymbol> declarationInfo; // In FUNC, it is declaration info of function scope.
}