package Classes;

import java.util.*;

import Util.TreeNode;
import Util.TreeNode.TreeNodeType;
import Classes.TableSymbol.ReturnType;
import Classes.TableSymbol.SymbolType;

public class AstNode extends Node {
	public enum AstNodeType {UNARY_PRE, UNARY_POST, BINARY, LEAF_VAR, LEAF_FUNC};
	//public enum ValueType {CHAR, INT, FLOAT, STRING};
	
	public static final String PLUS 	= "+";
	public static final String MINUS 	= "-";
	public static final String MULTI 	= "*";
	public static final String DIVIDE 	= "/";
	public static final String AND_OP	= "&&";
	public static final String OR_OP	= "||";
	public static final String EXCL 	= "!";
	//public static final String CARET 	= "^";
	
	public static final String INC 		= "++";
	public static final String DEC	 	= "--";
	
	public static final String ARRAY 	= "[]";
	
	public static final String ASSIGN 	= "=";
	public static final String EQUAL 		= "==";
	public static final String COMP_LE_EQ 	= ">=";
	public static final String COMP_RI_EQ	= "<=";
	public static final String COMP_LE		= ">";
	public static final String COMP_RI		= "<";

	public class ReturnFormat {
		public String 	nodeString;
		public int		nodeValue;
		
		public ReturnFormat (String nodeString, int nodeValue) {
			this.nodeString = nodeString;
			this.nodeValue = nodeValue;
		}
	}
	
	public AstNode (ReturnType type, Object value) {
		this.nodeType		= NodeType.AST;
		
		this.astNodeType	= AstNodeType.LEAF_VAR;
		this.valueType 		= type;
		this.nodeValue 		= value;
		this.leftNode		= null;
		this.rightNode		= null;
		this.funcArgVector	= null;
	}
	
	public AstNode (ReturnType type, Object value, Vector<AstNode> funcArgVector) {
		this.nodeType		= NodeType.AST;
		
		this.astNodeType	= AstNodeType.LEAF_FUNC;
		this.valueType 		= type;
		this.nodeValue 		= value;
		this.leftNode		= null;
		this.rightNode		= null;
		this.funcArgVector	= funcArgVector;
	}
	
	public AstNode (String value, AstNode childNode, Boolean isPost) {
		this.nodeType		= NodeType.AST;
		
		if (isPost) {
			this.astNodeType	= AstNodeType.UNARY_POST;
		}
		else {
			this.astNodeType	= AstNodeType.UNARY_PRE;
		}
		this.valueType 		= childNode.valueType;
		this.nodeValue 		= value;
		this.leftNode		= childNode;
		this.rightNode		= null;
		this.funcArgVector	= null;
	}
	
	public AstNode (String value, AstNode leftNode, AstNode rightNode) {
		if (!value.equals("[]")) {
			assert leftNode.valueType == rightNode.valueType;
		}
		this.nodeType		= NodeType.AST;
		
		this.astNodeType	= AstNodeType.BINARY;
		this.valueType 		= leftNode.valueType;
		this.nodeValue 		= value;
		this.leftNode		= leftNode;
		this.rightNode		= rightNode;
		this.funcArgVector	= null;
	}
	
	void updateVariable(SymbolTable table, int value, String varName) {
		TableSymbol symbol = table.findSymbol(varName, 0);
		if (symbol == null) {
			System.out.println("Symbol " + varName + "is not found! terminating...");
			System.exit(0);
		}
		if (symbol.symbolType != SymbolType.VARIABLE) {
			System.out.println("Symbol " + varName + " is not a variable! terminating...");
			System.exit(0);
		}
		AstNode valueNode = (AstNode)(symbol.value);
		valueNode.nodeValue = value;
	}
	
	void updateArray(SymbolTable table, int value, String arrayName, int index) {
		TableSymbol symbol = table.findSymbol(arrayName, 0);
		if (symbol == null) {
			System.out.println("Symbol " + arrayName + "is not found! terminating...");
			System.exit(0);
		}
		if (symbol.symbolType != SymbolType.ARRAY) {
			System.out.println("Symbol " + arrayName + "is not a array! terminating...");
			System.exit(0);
		}
		
		TableArrSymbol arrSymbol = (TableArrSymbol)symbol;

		AstNode nodeToFix = (AstNode)arrSymbol.arrayValueList.elementAt(index);
		nodeToFix.nodeValue = value;
	}
	
	void updateTable(SymbolTable table, AstNode nodeToUpdate, int value) {
		String lhsVariable = (String)nodeToUpdate.nodeValue;
		TableSymbol symbol;
		
		if ( (nodeToUpdate.astNodeType == AstNodeType.BINARY) && (lhsVariable.equals("[]")) ){
			String arrayVariable = (String)nodeToUpdate.leftNode.nodeValue;
			int index = nodeToUpdate.rightNode.evaluate(table, false).returnValue;
			updateArray(table, value, arrayVariable, index);
		}
		else if (nodeToUpdate.astNodeType == AstNodeType.LEAF_VAR) {
			symbol = table.findSymbol(lhsVariable, 0);
			if (symbol == null) {
				System.out.println("Symbol " + lhsVariable + " is not found from symbol table. terminating..");
				System.exit(0);
			}
			// 직접 수정, var이냐 array이냐에 따라서.
			updateVariable(table, value, lhsVariable);
		}
		else {
			System.out.println("Symbol " + lhsVariable + " can't be assigned. terminating..");
			System.exit(0);
		}
	}
	
	public TreeNode evaluate(SymbolTable table, Boolean forGraphGen) {
		TreeNode retNode = null;
		
		String key;
		TableSymbol symbol;
		//ReturnFormat content;
		
		TreeNode leftReturn  = null;
		TreeNode rightReturn = null;
		String 	retString 	= null;
		String 	operator 	= null;
		int 	retValue 	= Integer.MIN_VALUE;
		String  lhsVariable = null;
		
		switch (astNodeType) {
		case LEAF_VAR:
			switch (valueType) {
			case FLOAT:
				key = ((Float)nodeValue).toString();
				retNode = new TreeNode(key, ((Float)nodeValue).intValue());
				break;
			case INT:
				key = ((Integer)nodeValue).toString();
				retNode = new TreeNode(key, (Integer)nodeValue);
				break;
			case STRING:
				key = (String)nodeValue;
				symbol = table.findSymbol(key, 0);
				TreeNode evaluatedNode = ((AstNode)symbol.value).evaluate(table, forGraphGen);
				retNode = new TreeNode(key, evaluatedNode.returnValue);
				break;
			default:
				key = "default";
				retNode = new TreeNode(key, -1);
				break;
			}
			break;
		case LEAF_FUNC:
			key = (String)nodeValue;
			symbol = table.findSymbol(key, 0);
			if ( (symbol == null) || (symbol.symbolType != SymbolType.FUNC) ){
				System.out.println("Function symbol with name of " + key + " is not founded. terminating..");
				System.exit(0);
			}
			
			TableFuncSymbol funcSymbol = (TableFuncSymbol)symbol;
			String funcNameToPrint = key + "(";
			
			if (funcSymbol.argList != null) {
				// symbol.argList funcArgVector를 1:1 매칭시켜서 table에 입력.
				// arg가 var 또는 array일때를 고려..
				HashMap<String, TableSymbol> argScope = new HashMap<String, TableSymbol>();
				
				AstNode argVar;
				Iterator<AstNode> argVarIter = funcArgVector.iterator();
				TableSymbol curSymbol;
				TableVarSymbol argVarSymbol;
				TableArrSymbol argArrSymbol;
				AstNode internalValueNode;
				Iterator<TableSymbol> argSymbolIter = funcSymbol.argList.iterator();
				
				TreeNode evaluatedArgNode;
				while ( (argVarIter.hasNext()) && (argSymbolIter.hasNext())) {
					argVar = argVarIter.next();
					
					curSymbol = (TableSymbol)argSymbolIter.next();
					if (curSymbol.symbolType == SymbolType.VARIABLE) {
						argVarSymbol = (TableVarSymbol) curSymbol;
						evaluatedArgNode = argVar.evaluate(table, forGraphGen);
						funcNameToPrint += evaluatedArgNode.returnValue;
						
						// TODO: type checking?
						internalValueNode = (AstNode)argVarSymbol.value;
						internalValueNode.nodeValue = evaluatedArgNode.returnValue;
						
						argScope.put(argVarSymbol.key, argVarSymbol);
					}
					else if (curSymbol.symbolType == SymbolType.ARRAY) {
						argArrSymbol = (TableArrSymbol) curSymbol;
						if (argVar.astNodeType != AstNodeType.LEAF_VAR) {
							System.out.println("In function " + key + ", parameter " + curSymbol.key + " is not array! (it should be!)");
							System.exit(0);
						}
						TableArrSymbol arrParamSymbol = (TableArrSymbol) table.findSymbol((String)argVar.nodeValue, 0);
						funcNameToPrint += arrParamSymbol.arrayValueList.toString();
						argArrSymbol.arrayValueList = new Vector<AstNode>();
						argArrSymbol.arrayValueList.addAll(arrParamSymbol.arrayValueList);
						argArrSymbol.value = arrParamSymbol.value;
						
						argScope.put(argArrSymbol.key, arrParamSymbol);
					}
					else {
						System.out.println("In function " + key + ", parameter " + curSymbol.key + " is neither variable nor array!");
						System.exit(0);
					}
					
					if (argVarIter.hasNext()  && (argSymbolIter.hasNext()) ) {
						funcNameToPrint += ", ";
					}
				}
				funcNameToPrint += ")";
				
				if (forGraphGen == false) {
					System.out.println("Function " + funcNameToPrint + " is called!!");
				}
				table.pushScope(argScope);
				retNode = ((CompoundNode)symbol.value).evaluate(table, forGraphGen);
				table.popScope();
				retNode.singleValue = funcNameToPrint;
			}
			else {
				funcNameToPrint += ")";
				
				if (forGraphGen == false) {
					System.out.println("Function " + funcNameToPrint + " is called!!");
				}
				retNode = ((CompoundNode)symbol.value).evaluate(table, forGraphGen);
				retNode.singleValue = funcNameToPrint;
			}
			
			assert funcArgVector.size() == ((TableFuncSymbol)symbol).argList.size();
			
			
			if (forGraphGen == false) {
				if (retNode.returnValue != Integer.MIN_VALUE) {
					System.out.println("Function " + key + " is returned with value " + retNode.returnValue + "!");
				}
				else {
					System.out.println("Function " + key + " is returned with no value!");
				}
			}
			break;
//		case LEAF_ARRAY:
//			// TODO: for array
//			key = (String)nodeValue;
//			symbol = table.findSymbol(key, 0);
//			retNode = ((CompoundNode)symbol.value).evaluate(table, forGraphGen);
//			break;
		case UNARY_PRE:
		case UNARY_POST:
			leftReturn = leftNode.evaluate(table, forGraphGen);
			operator 	= (String)nodeValue;
			
			retString = leftReturn.singleValue + operator;
			retValue = leftReturn.returnValue;
			
			// distinguishing ++i & i++
			if (astNodeType == AstNodeType.UNARY_POST) {
				retNode = new TreeNode(retString, retValue);
			}
			
			if (operator.equals(INC)) {
				retValue += 1;
			}
			else if (operator.equals(DEC)) {
				retValue -= 1;
			}
			
			if (astNodeType == AstNodeType.UNARY_PRE) {
				retNode = new TreeNode(retString, retValue);
			}
			
			if (forGraphGen == false) {
				updateTable(table, leftNode, retValue);
			}
			break;
		case BINARY:
			leftReturn 	= leftNode.evaluate(table, forGraphGen);
			rightReturn = rightNode.evaluate(table, forGraphGen);
			operator 	= (String)nodeValue;
			lhsVariable = (String)leftNode.nodeValue;
			
			//retString = lhsVariable + operator + rightReturn.singleValue;
			if (operator.equals(ARRAY)) {
				retString = leftReturn.singleValue  + "[" + rightReturn.singleValue + "]";
			}
			else {
				retString = leftReturn.singleValue + operator + rightReturn.singleValue;
			}
			
			// calculate node value for each operator...
			if (operator.equals(AND_OP)) {
				/*
				if ( (leftReturn.nodeValue != 0) && (rightReturn.nodeValue != 0) ) {
					
				}
				*/
				retValue = leftReturn.returnValue & rightReturn.returnValue;
			}
			else if (operator.equals(OR_OP)) {
				retValue = leftReturn.returnValue | rightReturn.returnValue;
			}
			else if (operator.equals(MULTI)) {
				retValue = leftReturn.returnValue * rightReturn.returnValue;
			}
			else if (operator.equals(DIVIDE)) {
				retValue = leftReturn.returnValue / rightReturn.returnValue;
			}
			else if (operator.equals(PLUS)) {
				retValue = leftReturn.returnValue + rightReturn.returnValue;
			}
			else if (operator.equals(MINUS)) {
				retValue = leftReturn.returnValue - rightReturn.returnValue;
			}
			else if (operator.equals(EQUAL)) {
				retValue = (leftReturn.returnValue == rightReturn.returnValue) ? 1 : 0; 
			}
			else if (operator.equals(COMP_LE_EQ)) {
				retValue = (leftReturn.returnValue >= rightReturn.returnValue) ? 1 : 0; 
			}
			else if (operator.equals(COMP_LE_EQ)) {
				retValue = (leftReturn.returnValue <= rightReturn.returnValue) ? 1 : 0; 
			}
			else if (operator.equals(COMP_LE)) {
				retValue = (leftReturn.returnValue > rightReturn.returnValue) ? 1 : 0; 
			}
			else if (operator.equals(COMP_RI)) {
				retValue = (leftReturn.returnValue < rightReturn.returnValue) ? 1 : 0; 
			}
			else if (operator.equals(ASSIGN)) {
				retValue = rightReturn.returnValue;
				if (forGraphGen == false) {
					if (retValue == Integer.MIN_VALUE) {
						// TODO: revise check condition. 
						System.out.println("Return for " + leftReturn.singleValue + " is " + retValue + ": not matching!!");
						System.exit(0);
					}
					updateTable(table, leftNode, retValue);
				}
			}
			else if (operator.equals(ARRAY)) {
				symbol = table.findSymbol(lhsVariable, 0);
				if (symbol.symbolType != SymbolType.ARRAY) {
					System.out.println("Symbol " + lhsVariable + " is not an array. terminating..");
					System.exit(0);
				}
				TableArrSymbol arrSymbol = (TableArrSymbol)symbol;
				int index = rightReturn.returnValue;
				AstNode sizeNode = (AstNode)(arrSymbol.value);
				
				if (forGraphGen == false) {
					int arraySize = (Integer)(sizeNode.nodeValue);
					if (arraySize <= index) {
						System.out.println("Wrong index " + index + " on array symbol " + lhsVariable + " whose size is " + arraySize);
						System.exit(0);
					}
					AstNode rhsValueNode = arrSymbol.arrayValueList.elementAt(index);	// why AstNode? what about just Integer?
					
					retValue = rhsValueNode.evaluate(table, forGraphGen).returnValue;
				}
				else {
					retValue = Integer.MIN_VALUE; // dummy value
				}
			}
			
			retNode = new TreeNode(retString, retValue);
			if (rightReturn.type == TreeNodeType.COMP) {
				retNode.body = rightReturn;
			}
			
			break;
		}
		
		return retNode;
	}
	
	public String toString() {
		String retString = "";
		
		switch (valueType) {
		case VOID:
			retString += "void";
			break;
		case CHAR:
			retString += ((Character)nodeValue).toString();
			break;
		case INT:
			retString += ((Integer)nodeValue).toString();
			break;
		case FLOAT:
			retString += ((Float)nodeValue).toString();
			break;
		case STRING:
			retString += (String)nodeValue;
			break;
		}
		return retString;
	}
	
	public AstNodeType 		astNodeType;
	public ReturnType		valueType;
	public Object			nodeValue;
	public AstNode			leftNode;
	public AstNode			rightNode;
	public Vector<AstNode>	funcArgVector;
}
