import java.util.Vector;

public class AstNode {
	public enum AstNodeType {UNARY, BINARY, LEAF};
	public enum ValueType {CHAR, INT, FLOAT, STRING, VARIABLE, ARRAY};
	
	public static final String PLUS 	= "+";
	public static final String MINUS 	= "-";
	public static final String MULTI 	= "*";
	public static final String DIVIDE 	= "/";
	public static final String AND 		= "&";
	public static final String EXCL 	= "!";
	public static final String ARRAY 	= "[]";
	
	public AstNode (ValueType type, Object value) {
		this.nodeType		= AstNodeType.LEAF;
		this.valueType 		= type;
		this.nodeValue 		= value;
		this.leftNode		= null;
		this.rightNode		= null;
	}
	
	public AstNode (ValueType type, Object value, AstNode childNode) {
		this.nodeType		= AstNodeType.UNARY;
		this.valueType 		= type;
		this.nodeValue 		= value;
		this.leftNode		= childNode;
		this.rightNode		= null;
	}
	
	public AstNode (ValueType type, Object value, AstNode leftNode, AstNode rightNode) {
		this.nodeType		= AstNodeType.BINARY;
		this.valueType 		= type;
		this.nodeValue 		= value;
		this.leftNode		= leftNode;
		this.rightNode		= rightNode;
	}
	
	public Float calculate() {
		Float retValue = new Float(0);
		
		switch (nodeType) {
		case UNARY:
			
			break;
		case BINARY:
			
			break;
		case LEAF:
			
			break;
		default:
			
			break;
				
		}
		return retValue;
	}
	
	AstNodeType 	nodeType;
	ValueType		valueType;
	Object			nodeValue;
	AstNode			leftNode;
	AstNode			rightNode;
}
