import java.util.*;

public class SymbolTable {
	public SymbolTable() 
	{
		HashMap<String, TableSymbol> globalTable;
		tableVector = new Vector<HashMap<String, TableSymbol>>();
		
		globalTable = new HashMap<String, TableSymbol>();
		tableVector.add(globalTable);
	}
	
	public void insertSymbol(String key, int scope, TableSymbol symbol) 
	{
		HashMap<String, TableSymbol> tableAtScope = tableVector.elementAt(scope);
		tableAtScope.put(key, symbol);
	}
	
	public void insertSymbol(String key, int scope, TableSymbol.Type type, Object value) 
	{
		HashMap<String, TableSymbol> tableAtScope = tableVector.elementAt(scope);
		TableSymbol symbol = new TableSymbol(key, scope, type, value);
		tableAtScope.put(key, symbol);
	}
	
	public void insertSymbol(String key, TableSymbol.Type type, Object value, 
								TableSymbol.Type returnType, Vector<TableSymbol.ArgNode> argVector, int scope) 
	{
		if (type.typeValue == TableSymbol.SymbolType.FUNC) {
			System.out.println("Error in insertSymbol: Use other constructor for func!");
			return;
		}
		
		HashMap<String, TableSymbol> tableAtScope = tableVector.elementAt(scope);
		TableSymbol symbol = new TableSymbol(key, scope, type, value, returnType, argVector);
		
		System.out.println("SymbolTable::insertSymbol: symbol " + symbol.toString() + " is inserted!");
		tableAtScope.put(key, symbol);
	}
	
	public TableSymbol findSymbol(String name, int scope) 
	{
		HashMap<String, TableSymbol> tableAtScope = tableVector.elementAt(scope);
		TableSymbol ret = null;
		
		// find symbol from tableStack (from top of Stack);
		ret = tableAtScope.get(name);
		
		return ret;
	}
	
	public void popScope() {
		// delete most recent scope stack. global stack shouldn't be popped.
		if (tableVector != null && (tableVector.size() != 1)) {
			tableVector.removeElementAt(tableVector.size()-1);
		}
		else {
			if (tableVector.size() == 1) {
				System.out.println("Error, SymbolTable::popScope(): why popping global table?");
			}
			else {
				System.out.println("Error, SymbolTable::popScope(): no scope to pop!");
			}
		}
	}
	
	private Vector<HashMap<String, TableSymbol>> tableVector;
}
