import java.io.*;
import java_cup.runtime.*;

import Classes.SymbolTable;

import Classes.CompoundNode;
import Classes.TableSymbol;
import Classes.TableSymbol.SymbolType;
import Classes.TableFuncSymbol;

import javax.swing.JFrame;
import Util.Visualizer;
import Util.TreeNode;

public class Main {
    public static void main( String[] argv )
    throws java.io.IOException, java.lang.Exception {
        
        try {
            //Parser p = new Parser( new Lexer( new FileReader( "test1.c" ) ) );
            Parser p = new Parser( new Lexer( new FileReader( argv[0] ) ) );
            
            //p.parse();
            p.debug_parse(); // For debugging
            
            SymbolTable table = p.action_obj.table;
            
            //// For visualization
           	Visualizer frame = new Visualizer();
           	frame.visualizeTree(table);
       		
           	frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
           	frame.setSize(800, 320);
           	frame.setVisible(true);
           	
            System.out.println("Parsing done!");
        }
        catch (java.io.FileNotFoundException e) {
            System.out.println("File not found : \""+argv[0]+"\"");
            System.exit(1);
        }
        catch (java.io.IOException e) {
            System.out.println("Error opening file \""+argv[0]+"\"");
            System.exit(1);
        }
        catch (ArrayIndexOutOfBoundsException e) {
            System.out.println("Usage : java Main <inputfile>");
            System.exit(1);
        }
        catch ( Exception e ) {
            System.err.println( "Exception at " );
            e.printStackTrace();
        }
    }
}